/**
 * Created by benevideschissanga on 9/14/15.
 */
$(document).ready(function () {
    if ($('select[id="lista_areas"]').length > 0) {
        var lista = $('select[id="lista_areas"]').bootstrapDualListbox({
            moveOnSelect: false,
            moveAll: false,
            showFilterInputs: false,
            infoText: false,
            moveSelectedLabel: 'adicionar',
            moveAllLabel: 'adicionar todas',
            removeAllLabel: 'Remover todas',
            removeSelectedLabel: 'Remover'
        });
    }


  $(function () {
    $('.datetimepicker').datetimepicker({
      format: 'LT',
      defaultDate: moment(),
      locale: 'pt',
      icons: {
        time: 'fa fa-time',
        date: 'fa fa-calendar',
        up: 'fa fa-caret-up',
        down: 'fa fa-caret-down',
        close: 'fa fa-close'
      },
      inline: true
    });
  });

});
