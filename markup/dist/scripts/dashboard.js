/**
 * Created by benevideschissanga on 8/16/15.
 */
$(document).ready(function () {
    //charts
    if ($('.total-wrapper').length) {
        //total de dados
        var total = 130;
        //total de homens
        var homens = 100;
        //total de mulheres
        var mulheres = 30;
        //mostrar os dados na página
        $(".men-total").html(homens);
        $(".women-total").html(mulheres);
        $(".total-pacientes").html(total);
        //dados do chart_1
        var doughnutData = [
            {
                value: homens,
                color: "#2ca8ff",
                highlight: "#FF5A5E",
                label: "Homens"
            },
            {
                //the amount that represents 'unskilled' percentage
                value: total - homens,
                color: 'gray'
            }
        ];

        //dados do chart_2
        var doughnutData1 = [
            {
                value: mulheres,
                color: "#2ca8ff",
                highlight: "#FF5A5E",
                label: "Mulheres"
            }, {
                //the amount that represents 'unskilled' percentage
                value: total - mulheres,
                color: 'gray'
            }
        ];
        //chart_1
        var ctx = $('#chart-area').get(0).getContext("2d");
        var myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
            animation: true,
            responsive: true,
            showTooltips: false,
            percentageInnerCutout: 70,
            segmentShowStroke: true,
            maintainAspectRatio: true,
            onAnimationComplete: function () {

                var canvasWidthvar = $('#chart-area').width();
                var canvasHeight = $('#chart-area').height();
                //this constant base on canvasHeight / 2.8em
                var constant = 114;
                var fontsize = (canvasHeight / constant).toFixed(2);
                ctx.font = fontsize + "em ralewayregular";
                ctx.textBaseline = "middle";
                var tpercentage = ((doughnutData[0].value * 100) / total).toFixed(1) + "%";
                var textWidth = ctx.measureText(tpercentage).width;

                var txtPosx = Math.round((canvasWidthvar - textWidth) / 2);
                ctx.fillText(tpercentage, txtPosx, canvasHeight / 2);
            }
        });

        //chart_2
        var ctx1 = $('#chart-area1').get(0).getContext("2d");
        var myDoughnut1 = new Chart(ctx1).Doughnut(doughnutData1, {
            animation: true,
            responsive: true,
            showTooltips: false,
            percentageInnerCutout: 70,
            segmentShowStroke: true,
            onAnimationComplete: function () {

                var canvasWidthvar = $('#chart-area1').width();
                var canvasHeight = $('#chart-area1').height();
                //this constant base on canvasHeight / 2.8em
                var constant = 114;
                var fontsize = (canvasHeight / constant).toFixed(2);
                ctx1.font = fontsize + "em ralewayregular";
                ctx1.textBaseline = "middle";
                var tpercentage = ((doughnutData1[0].value * 100) / total).toFixed(1) + "%";
                var textWidth = ctx1.measureText(tpercentage).width;

                var txtPosx = Math.round((canvasWidthvar - textWidth) / 2);
                ctx1.fillText(tpercentage, txtPosx, canvasHeight / 2);
            }
        });
    }
    if ($('.charts').length) {
        //Chart de Consultas
        $(function () {
            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'consulta-chart',
                    type: 'bar',
                    animation: true,
                    spacingTop: 0,
                    spacingRight: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    plotBorderWidth: 0,
                    marginRight: 0,//-60, //this does move the chart but you'll need to recompute it
                    marginLeft: 0,//-60,  //whenever the page changes width
                    marginTop: 0,
                    marginBottom: 0,
                    style: {
                        fontFamily: 'ralewayregular',
                        textShadow: false
                    }
                },
                title: {
                    text: ''

                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    tickheight: 0,
                    tickWidth: 0,
                    tickPosition: 'outside',
                    categories: ['Consultas'],
                    gridLineWidth: 0,
                    labels: {enabled: false}
                },
                yAxis: {
                    enabled: false,
                    min: 0,
                    title: {
                        text: ''
                    },
                    gridLineWidth: 0,
                    labels: {enabled: false}
                },
                legend: {
                    reversed: true,
                    enabled: true,
                    floating: true,
                    verticalAlign: 'bottom',
                    align: 'left'
                },

                plotOptions: {
                    series: {
                        stacking: 'normal'
                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            formatter: function () {
                                var dlabel = this.series.name + '<br/>';
                                var percentagem = Math.round(this.percentage * 100) / 100;
                                dlabel += percentagem.toFixed(1) + ' %';
                                return dlabel;
                            },
                            style: {
                                color: 'white',fontSize: '13px', textShadow: false
                            }
                        }

                    }
                },
                series: [{
                    name: 'Realizadas',
                    data: [100],
                    color: '#2ca8ff'
                }, {
                    name: 'Pendentes',
                    data: [40],
                    color: 'gray'
                }]
            });
            //Mostra o número total de consultas
            var consultas = chart.series[0].dataMax;
            $(".total-consultas").html(consultas);

        });
        //chart distribuição etaria
        $(function () {
            // Age categories
            var categories = ['0-4', '5-9', '10-14', '15-19',
                '20-24', '25-29', '30-34', '35-39', '40-44',
                '45-49', '50-54', '55-59', '60-64', '65-69',
                '70-74', '75-79', '80-84', '85-89', '90-94',
                '95-99', '100 + '];
            $('#dist-etaria').highcharts({
                chart: {
                    type: 'bar',
                    animation: true,
                    style: {
                        fontFamily: 'ralewayregular',
                        textShadow: false
                    }
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: [{
                    categories: categories,
                    reversed: false,
                    labels: {
                        step: 1
                    }
                }, { // mirror axis on right side
                    opposite: true,
                    reversed: false,
                    categories: categories,
                    linkedTo: 0,
                    labels: {
                        step: 1
                    }
                }],
                yAxis: {
                    title: {
                        text: null
                    },
                    labels: {
                        formatter: function () {
                            return Math.abs(this.value) + '%';
                        }
                    }
                },

                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + ', idade ' + this.point.category + '</b><br/>' +
                            'Agendamentos: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
                    }
                },

                series: [{
                    name: 'Homens',
                    data: [-2.2, -2.2, -2.3, -2.5, -2.7, -3.1, -3.2,
                        -3.0, -3.2, -4.3, -4.4, -3.6, -3.1, -2.4,
                        -2.5, -2.3, -1.2, -0.6, -0.2, -0.0, -0.0],
                    color: '#2ca8ff'
                }, {
                    name: 'Mulheres',
                    data: [2.1, 2.0, 2.2, 2.4, 2.6, 3.0, 3.1, 2.9,
                        3.1, 4.1, 4.3, 3.6, 3.4, 2.6, 2.9, 2.9,
                        1.8, 1.2, 0.6, 0.1, 0.0],
                    color: 'black'
                }]
            });


        });
        $(function () {
            var options = {
                chart: {
                    renderTo: 'agend-periodo',
                    defaultSeriesType: 'spline',
                    style: {
                        fontFamily: 'ralewayregular',
                        textShadow: false
                    },
                    animation: true
                },
                plotOptions: {
                    spline: {
                        lineWidth: 4,
                        states: {
                            hover: {
                                lineWidth: 5
                            }
                        }
                    }
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: {

                    allowDecimals: false,
                    categories: ['Dom', 'Seg', 'Ter', 'Quar', 'Qui', 'Sex', 'Sáb'],
                    max: 6
                },

                yAxis: {
                    allowDecimals: false,
                    align: 'left',
                    x: 0,
                    y: -2
                },
                series: [{name: 'Esta semana', data: [1, 2, 3, 2, 1]}]
            };
            var chart = new Highcharts.Chart(options);

            $("#list").on('change', function () {
                var selVal = $("#list").val();
                if (selVal == "Esta semana" || selVal == '') {
                    options.series = [{name: 'Esta semana', data: [1, 2, 3, 2, 1, 4, 5]}];
                    options.xAxis = [{categories: ['Dom', 'Seg', 'Ter', 'Quar', 'Qui', 'Sex', 'Sáb']}];
                }
                else if (selVal == "Este mês") {
                    options.series = [{name: 'Este mês', data: [3, 2, 1, 2, 3]}];
                    options.xAxis = [{categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']}];
                }
                else if (selVal == "Este ano") {
                    options.series = [{name: 'Este ano', data: [5, 4, 8, 7, 6]}];
                    options.xAxis = [{categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']}];
                }
                var chart = new Highcharts.Chart(options);
            });
        });
    }
});
