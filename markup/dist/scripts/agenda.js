/**
 * Created by benevideschissanga on 8/17/15.
 */
$(document).ready(function () {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /*  className colors

     className: default(transparent), important(red), chill(pink), success(green), info(blue)

     */


    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });


    /* initialize the calendar
     -----------------------------------------------------------------*/

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,next today'
        },
        lang: 'pt',
        editable: true,
        eventLimit: true,
        firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
        selectable: true,
        defaultView: 'agendaDay',

        axisFormat: 'h:mm',
        columnFormat: {
            month: 'ddd',    // Mon
            week: 'ddd d', // Mon 7
            day: 'dddd M/d',  // Monday 9/7
            agendaDay: 'dddd D'
        },
        displayEventEnd: {
            month: false,
            basicWeek: true,
            "default": true
        },
        titleFormat: {
            month: 'MMMM YYYY', // September 2009
            week: "MMMM YYYY", // September 2009
            day: 'MMMM YYYY'                  // Tuesday, Sep 8, 2009
        },
        select: function (start, end) {
            var starttime = moment(event.start);
            var endtime = moment(event.end);
            var mywhen = starttime + ' - ' + endtime;
            $('#modal-novo-envento #data').val(start);
            $('#modal-novo-envento #hora').val(end);
            $('#modal-novo-envento #when').text(mywhen);
            $('#modal-novo-envento').modal('show');
        },
        eventClick: function (event, jsEvent, view) {
            $('#modalTitle').html(event.title);
            $('#modalBody').html(event.description + "<br/>" + moment(event.start).format('MMM Do h:mm A'));
            $('#eventUrl').attr('href', event.url);
            $('#modal-mostrar-evento').modal();
        },
        eventRender: function (event, element) {
            element.find('.fc-title').append("<br/>" + event.description + "<br/>" + moment(event.start).format('MMM Do h:mm A'));
        },
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },

        events: [
            {
                title: 'All Day Event',
                start: new Date(y, m, 1)
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false,
                className: 'info'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false,
                className: 'info'
            },
            {
                title: 'Meeting',
                start: new Date(y, m, d, 10, 30),
                allDay: false,
                className: 'important'
            },
            {
                title: 'Lunch',
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d, 14, 0),
                allDay: false,
                className: 'important'
            },
            {
                title: 'Birthday Party',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false,
            },
            {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://google.com/',
                className: 'success'
            }
        ]
    });
    $('#submitButton').on('click', function (e) {
        doSubmit();
    });
    $('.fc-toolbar .fc-right').prepend(
        $('<div class="fc-button-group"><button type="button" class="fc-button fc-state-default fc-corner-left" data-toggle="modal" data-target="#modal-procurar"><i class="pe-7s-search pe-lg"></i></button><button type="button" class="fc-button fc-state-default  fc-corner-right"><i class="pe-7s-attention pe-lg"></i> </button></div>')
    );

    function doSubmit() {
        $("#createEventModal").modal('hide');
        console.log($('#apptStartTime').val());
        console.log($('#apptEndTime').val());
        $("#calendar").fullCalendar('renderEvent',
            {
                title: $('#patientName').val(),
                start: $('#data').val(),
                end: $('#hora').val(),
                description: $('#eventdescription').val()
            });

    }
    //picker de horas
    $(function () {
        $('.datetimepicker').datetimepicker({
            format: 'LT',
            showClose: true,
            defaultDate: moment()
        });
    });
});
