/**
 * Created by benevideschissanga on 8/21/15.
 */
var $table = $('table[id*="fresh-table"]');
var $table_no_option = $('.tabela-sem-opcoes');
$().ready(function () {
    $table.bootstrapTable({
        showRefresh: true,
        search: true,
        pagination: true,
        sortable: true,
        pageSize: 8,

        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            //do nothing here, we don't want to show the text "showing x of y from..."
        },
        formatRecordsPerPage: function (pageNumber) {
            return pageNumber + " Itens por página";
        },
        icons: {
            refresh: 'fa fa-refresh',
            toggle: 'fa fa-th-list',
            columns: 'fa fa-columns',
            detailOpen: 'fa fa-plus-circle',
            detailClose: 'fa fa-minus-circle'
        }
    });
});

$().ready(function () {
    $table_no_option.bootstrapTable({
        pagination: true,
        pageSize: 8,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            //do nothing here, we don't want to show the text "showing x of y from..."
        },
        formatRecordsPerPage: function (pageNumber) {
            return pageNumber + " Itens por página";
        }
    });
});

function operateFormatter(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Detalhes" class="table-action btn btn-simple btn-info" href="detalhe-paciente.html">',
        '<i class="fa fa-bars"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Apagar" class="table-action btn btn-simple btn-danger" href="#">',
        '<i class="fa fa-trash"></i>',
        '</a>'
    ].join('');
}
function operateFormatter1(value, row, index) {
    return [
        '<a class="table-action btn btn-simple btn-info" data-toggle="popover" data-placement="top" title="Observações" data-content="Aqui irão algumas informações muito úteis."><i class="fa fa-info-circle"></i></a>',
        '<a data-tooltip="tooltip" title="Atendimento" class="table-action btn btn-simple btn-danger" data-toggle="modal" data-target="#modal-atendimento">',
        '<i class="fa fa-heartbeat"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Resumo" class="table-action btn btn-simple" href="#" data-toggle="modal" data-target="#modal-resumo-atendimento">',
        '<i class="fa fa-file-text"></i>',
        '</a>'
    ].join('');
}
function operateFormatter2(value, row, index) {
    return [
        '<a class="table-action btn btn-simple btn-info" data-toggle="popover" data-placement="top" title="Observações" href="#" data-content="Aqui irão algumas informações muito úteis."><i class="fa fa-info-circle"></i></a>',
        '<a data-tooltip="tooltip" title="Novo atendimento" class="table-action btn btn-simple btn-danger" href="novo-atendimento.html">',
        '<i class="fa fa-stethoscope"></i>',
        '</a>'
    ].join('');
}

function operateFormatter3(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Detalhes" class="table-action btn btn-simple btn-info" href="detalhe-exame.html">',
        '<i class="fa fa-bars"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Apagar" class="table-action btn btn-simple btn-danger" href="#">',
        '<i class="fa fa-trash"></i>',
        '</a>'
    ].join('');
}
function operateFormatter4(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Detalhes" class="table-action btn btn-simple btn-info" href="detalhe-internamentos.html">',
        '<i class="fa fa-bars"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Apagar" class="table-action btn btn-simple btn-danger" href="#">',
        '<i class="fa fa-trash"></i>',
        '</a>'
    ].join('');
}
function operateFormatter5(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Confirmar pagamento" class="table-action confirmar btn btn-simple btn-success" href="#" data-toggle="modal" data-target="#modal-confirmar-pagamento">',
        '<i class="fa fa-check-circle-o"></i>',
        '</a>'
    ].join('');
}
function operateFormatter6(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Editar" class="table-action btn btn-simple btn-info" data-toggle="modal" data-target="#adicionar-receita-despesa" href="#">',
        '<i class="fa fa-pencil"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Eliminar Receita" class="table-action btn btn-simple btn-danger" href="#" data-toggle="modal" data-target="#modal-eliminar-receita-despesa">',
        '<i class="fa fa-ban"></i>',
        '</a>'
    ].join('');
}
function operateFormatter7(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Detalhes" class="table-action btn btn-simple btn-info" href="detalhe-funcionario.html">',
        '<i class="fa fa-bars"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Apagar" class="table-action btn btn-simple btn-danger" href="#">',
        '<i class="fa fa-trash"></i>',
        '</a>'
    ].join('');
}
function operateFormatter8(value, row, index) {
    return [
        '<a data-tooltip="tooltip" title="Detalhes" class="table-action btn btn-simple btn-info" href="detalhe-funcionario.html">',
        '<i class="fa fa-bars"></i>',
        '</a>',
        '<a data-tooltip="tooltip" title="Remover hora extra" class="table-action btn btn-simple btn-danger" href="#">',
        '<i class="fa fa-minus-circle"></i>',
        '</a>'
    ].join('');
}
function stateFormatter(value, row, index) {
    if (index === 2) {
        return {
            disabled: true
        };
    }
    if (index === 5) {
        return {
            disabled: true,
            checked: true
        }
    }
    return value;
}
$(function () {
    $('[data-toggle="popover"]').popover({trigger: "hover"});
});

if ($('.tabela-with-advancedSearch').length) {
    setTimeout(function () {
        var botaoPesquisaAvancada = $(".fixed-table-toolbar .columns.columns-right");
        botaoPesquisaAvancada.prepend('<a href="#" class="btn btn-default" type="button" data-tooltip="tooltip" data-toggle="modal" data-target="#modal-pesquisa-avancada" title="pesquisa avançada"><i class="fa fa-search-plus"></i></a>');
    }, 100);
}
