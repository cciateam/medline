/**
 * Created by benevideschissanga on 9/5/15.
 */
//pie chart finanças
$(function () {
    $('#pie').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0
        },
        style: {
            fontFamily: 'ralewayregular'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                size:'100%',
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return this.point.name + '<br>' + Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -90,
                    color:'white',
                    style: {fontSize: '16px', textShadow: false }
                }
            }
        },
        series: [{
            name: "Transações",
            colorByPoint: true,
            data: [{
                name: "Receitas",
                y: 56.33,
                color: '#6AC007'
            }, {
                name: "Despesas",
                y: 24.03,
                sliced: true,
                selected: true,
                color: '#EE2D20'
            }]
        }]
    });
});
