﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MedLine.Startup))]
namespace MedLine
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
