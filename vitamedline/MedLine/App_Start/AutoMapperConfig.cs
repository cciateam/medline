﻿using AutoMapper;
using MedLine.Models.Comum.ViewModel;
using MedLine.Models.Laboratorio;
using MedLine.Models.Laboratorio.ViewModel;
using MedLine.Models.Pacientes;
using MedLine.Services.ComumServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedLine.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AnaliseExameLaboratorio, ExamesViewModel>()
                    .ForMember(a => a.Id, o => o.MapFrom(x => x.Id))
                    .ForMember(a => a.CodigoPaciente, o => o.MapFrom(x => x.Paciente.Codigo))
                    .ForMember(a => a.NomePaciente, o => o.MapFrom(x => x.Paciente.Nome))
                    .ForMember(a => a.CID, o => o.MapFrom(x => x.AreaActuacao.CID))
                    .ForMember(a => a.ExameDescricao, o => o.MapFrom(x => x.AreaActuacao.Nome + " | " + x.AreaActuacao.TipoAgendamento.Nome))
                    .ForMember(a => a.DataRegisto, o => o.MapFrom(x => x.DataRegisto.ToString("dd-MM-yyyy")))
                    .ForMember(a => a.HoraRegisto, o => o.MapFrom(x => x.DataRegisto.ToString("HH:mm")))
                    .ForMember(a => a.Estado, o => o.MapFrom(x => Extensions.GetDescription(x.EstadoAnaliseExame)))
                    .ForMember(a => a.EEstado, o => o.MapFrom(x => x.EstadoAnaliseExame));

                cfg.CreateMap<Paciente, PacientesViewModel>()
                    .ForMember(a => a.Id, o => o.MapFrom(x => x.Id))
                    .ForMember(a => a.CodigoPaciente, o => o.MapFrom(x => x.Codigo))
                    .ForMember(a => a.NomePaciente, o => o.MapFrom(x => x.Nome))
                    .ForMember(a => a.Idade, o => o.MapFrom(x => new AgeServices(x.DataNascimento).Years + " ano(s)"))
                    .ForMember(a => a.Sexo, o => o.MapFrom(x => x.Sexo.Nome))
                    .ForMember(a => a.Telefone, o => o.MapFrom(x => x.Telefone))
                    .ForMember(a => a.TipoCliente, o => o.MapFrom(x => Extensions.GetDescription(x.TipoPaciente)));
            });
        }
    }
}