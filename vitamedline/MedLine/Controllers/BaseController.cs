﻿using MedLine.Models.Comum;
using MedLine.Models.Enums;
using MedLine.Models.Financa;
using MedLine.Models.Identity;
using MedLine.Models.Laboratorio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedLine.Controllers
{
    public class BaseController : Controller
    {
        private AppDbContext db = new AppDbContext();

        protected List<Ficheiro> SessionFile
        {
            get
            {
                if (Session["myFiles"] == null)
                    Session["myFiles"] = new List<Ficheiro>();
                return (List<Ficheiro>)Session["myFiles"];
            }
            set { Session["myFiles"] = value; }
        }

        protected void SaveFicheiros(int id)
        {
            foreach (var item in SessionFile)
            {
                Ficheiro file = new Ficheiro
                {
                    Descricao = item.Descricao,
                    File = item.File,
                    TipoFicheiro = item.TipoFicheiro
                };
                db.Ficheiros.Add(file);
                db.SaveChanges();
                if (item.TipoFicheiro == ETipoFicheiro.Laboratorio)
                {
                    SaveFicheiroExameLaboratorio(file, id);
                }
                else
                {
                    SaveFicheiroPagamento(file, id);
                }
            }
        }
        private void SaveFicheiroExameLaboratorio(Ficheiro ficheiro, int id)
        {
            AnaliseExameFicheiro model = new AnaliseExameFicheiro
            {
                FicheiroId = ficheiro.Id,
                AnaliseExameId = id
            };
            db.AnalisesExamesFicheiros.Add(model);
            db.SaveChanges();
        }
        private void SaveFicheiroPagamento(Ficheiro ficheiro, int id)
        {
            PagamentoFicheiro model = new PagamentoFicheiro
            {
                FicheiroId = ficheiro.Id,
                PagamentoId = id
            };
            db.PagamentosFicheiros.Add(model);
            db.SaveChanges();
        }
        public FileResult Download(int id)
        {
            Ficheiro fileBytes = db.Ficheiros.First(o => o.Id == id);
            //byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\folder\myfile.ext");
            string fileName = fileBytes.Descricao;
            return File(fileBytes.File, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

    }
}