﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Consulta;
using MedLine.Models.Identity;
using MedLine.Services.AgendamentoServices;
using System.Data.Entity;
using MedLine.Services.ComumServices;
using System.Globalization;
using MedLine.Models.Financa;
using MedLine.Services.FinancaServices;

namespace MedLine.Controllers
{
    public class AgendamentoesController : Controller
    {
        private AppDbContext db = new AppDbContext();
        private VitaServices vitaServices = new VitaServices();
        private AgendamentoServices agendamentoservice = new AgendamentoServices();
        private PagamentosServices pagamentosServices = new PagamentosServices();


        public ActionResult AgendamentosEventsJson()
        {
            var agendamentos = db.Agendamentos.Include(t => t.Paciente).Include(t => t.Funcionario).Include(t => t.AreaActuacao).ToList();

            return Json(agendamentos.Select(x => new { title = x.Paciente.Nome, observacoes = x.Observacoes, id = x.Id,  area = x.AreaActuacao.Nome, funcionario = x.Funcionario.Nome, color = VitaServices.ColorFromState( x.Estado), start = x.DataConsulta.ToString("o", CultureInfo.InvariantCulture), end = x.DataConsulta.ToString("o", CultureInfo.InvariantCulture) }), JsonRequestBehavior.AllowGet);

        }

        public ActionResult PesquisaAgendamentos()
        {
            var agendamentos = db.Agendamentos.Include(t => t.Paciente).Include(t => t.Funcionario).Include(t => t.AreaActuacao).ToList();
            return View(agendamentos);
        }

        // GET: Agendamentoes
        public ActionResult Index()
        {
            var agendamentos = db.Agendamentos.Include(a => a.AreaActuacao).Include(a => a.Funcionario).Include(a => a.Paciente);
            return View(agendamentos.ToList());
        }

        // GET: Agendamentoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agendamento agendamento = db.Agendamentos.Find(id);
            if (agendamento == null)
            {
                return HttpNotFound();
            }
            return View(agendamento);
        }

        // GET: Agendamentoes/Create
        public ActionResult Create()
        {
            ViewBag.AreaActuacaoId = new SelectList(db.AreasActuacoes, "Id", "Nome");
            ViewBag.FuncionarioId = new SelectList(db.Funcionarios, "Id", "Nome");
            ViewBag.PacienteId = new SelectList(db.Pacientes, "Id", "Nome");
            return View();
        }

        // POST: Agendamentoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PacienteId,AreaActuacaoId,FuncionarioId,Estado,DataRegisto,DataConsulta,Codigo,Sequencia,Observacoes")] Agendamento agendamento, string horaAgendamento)
        {
            int sequencia = 0;
            var codigo = agendamentoservice.GenerateCodigo(out sequencia);

           
            agendamento.DataRegisto = DateTime.Now;
            agendamento.Estado = Models.Enums.EEstadoAgendamento.Agendado;
            agendamento.Sequencia = sequencia;
            agendamento.Codigo = codigo;
            agendamento.DataEstado = DateTime.Now;

            int h = Convert.ToInt32(horaAgendamento.Split(new[]{':'})[0]);
            int m = Convert.ToInt32(horaAgendamento.Split(new[] { ':' })[1]);

            var hora = new TimeSpan(h, m, 0);
            agendamento.DataConsulta = agendamento.DataConsulta.Add(hora);

            if (ModelState.IsValid)
            {
                db.Agendamentos.Add(agendamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: Agendamentoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agendamento agendamento = db.Agendamentos.Find(id);
            if (agendamento == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaActuacaoId = new SelectList(db.AreasActuacoes, "Id", "Nome", agendamento.AreaActuacaoId);
            ViewBag.FuncionarioId = new SelectList(db.Funcionarios, "Id", "Nome", agendamento.FuncionarioId);
            ViewBag.PacienteId = new SelectList(db.Pacientes, "Id", "Nome", agendamento.PacienteId);
            return View(agendamento);
        }

        // POST: Agendamentoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PacienteId,AreaActuacaoId,FuncionarioId,Estado,DataRegisto,DataConsulta,Codigo,Sequencia,Observacoes")] Agendamento agendamento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(agendamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaActuacaoId = new SelectList(db.AreasActuacoes, "Id", "Nome", agendamento.AreaActuacaoId);
            ViewBag.FuncionarioId = new SelectList(db.Funcionarios, "Id", "Nome", agendamento.FuncionarioId);
            ViewBag.PacienteId = new SelectList(db.Pacientes, "Id", "Nome", agendamento.PacienteId);
            return View(agendamento);
        }

        // GET: Agendamentoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agendamento agendamento = db.Agendamentos.Find(id);
            if (agendamento == null)
            {
                return HttpNotFound();
            }
            return View(agendamento);
        }

        // POST: Agendamentoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Agendamento agendamento = db.Agendamentos.Find(id);
            db.Agendamentos.Remove(agendamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agendamento agendamento = db.Agendamentos.Find(id);
            if (agendamento == null)
            {
                return HttpNotFound();
            }
            return View(agendamento);
        }

        // POST: Agendamentoes/Confirm/5
        [HttpPost, ActionName("Confirm")]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmOk(int id)
        {

            Agendamento agendamento = db.Agendamentos.Find(id);

            int sequencia = 0;
            var codigo = pagamentosServices.GenerateCodigo(out sequencia);

            // gera o pagamento
            Pagamento pagamento = new Pagamento
            {
                AgendamentoId = agendamento.Id,
                DataRegisto = DateTime.Now,
                Pago = false,
                SubTotal = agendamento.AreaActuacao.Valor,
                Total = agendamento.AreaActuacao.Valor,
                Sequencia = sequencia,
                Codigo = codigo
            };

            

            db.Pagamentos.Add(pagamento);
            db.SaveChanges();

            PagamentoDetalhes pgDetalhe = new PagamentoDetalhes();
            pgDetalhe.AreaActuacaoId = agendamento.AreaActuacaoId;
            pgDetalhe.PagamentoId = pagamento.Id;
            pgDetalhe.Valor = agendamento.AreaActuacao.Valor;

            db.PagamentosDetalhes.Add(pgDetalhe);
            db.SaveChanges();

            
            agendamento.Estado = Models.Enums.EEstadoAgendamento.Confirmado;
            db.Entry(agendamento).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
