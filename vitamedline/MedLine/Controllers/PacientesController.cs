﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Identity;
using MedLine.Models.Pacientes;
using MedLine.Services.PacienteServices;
using MedLine.Models.Enums;

namespace MedLine.Controllers
{
    public class PacientesController : BaseController
    {
        private AppDbContext db = new AppDbContext();
        PacienteServices pacienteServices = new PacienteServices();

        // GET: Pacientes
        public ActionResult Index()
        {
            var pacientes = db.Pacientes.Include(p => p.Parentesco).Include(p => p.Seguradora).Include(p => p.Sexo);
            return View(pacientes.ToList());
        }

        public ActionResult JsonList()
        {
            var pacientes = db.Pacientes;

            return Json(pacientes.Select(x=> new { Codigo = x.Codigo, Nome = x.Nome }), JsonRequestBehavior.AllowGet);
        }

        // GET: Pacientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        public PartialViewResult _ResumoAnaliseMedica(int id)
        {
            var analise = db.Analises.FirstOrDefault(x => x.AgendamentoId == id);
            return PartialView(analise);
        }
        public PartialViewResult _ResumoPrescricaoMedica(int id)
        {
            var analise = db.Analises.FirstOrDefault(x => x.AgendamentoId == id);
            return PartialView(analise);
        }

        // GET: Pacientes/Create
        public ActionResult Create()
        {
            ViewBag.ParentescoId = new SelectList(db.Parentescos, "Id", "Nome");
            ViewBag.SeguradoraId = new SelectList(db.Seguradoras, "Id", "Nome");
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome");
            ViewBag.SangueId = new SelectList(db.Sangues, "Id", "Nome");
            ViewBag.Codigo = pacienteServices.GenerateCodigo();

            return View();
        }

        // POST: Pacientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Paciente paciente)
        {
            if (paciente.TipoPaciente == ETipoPaciente.SeguroEmpresa)
            {
                if (paciente.SeguradoraId == null)
                {
                    ModelState.AddModelError("", "Deve seleccionar a seguradora");
                }
                if (string.IsNullOrEmpty(paciente.Apolice))
                {
                    ModelState.AddModelError("", "Deve preencher o número do cartão da apólice");
                }
                if (!paciente.DataValidade.HasValue)
                {
                    ModelState.AddModelError("", "Deve preencher a data de validade");
                }
                if (string.IsNullOrEmpty(paciente.Empresa))
                {
                    ModelState.AddModelError("", "Para tipos de pacientes com seguro empresa, deve indicar a empresa. ");
                }
            }
            else if (paciente.TipoPaciente == ETipoPaciente.SeguroParticular)
            {
                if (paciente.SeguradoraId == null)
                {
                    ModelState.AddModelError("", "Deve seleccionar a seguradora");
                }
                if (string.IsNullOrEmpty(paciente.Apolice))
                {
                    ModelState.AddModelError("", "Deve preencher o número do cartão da apólice");
                }
                if (!paciente.DataValidade.HasValue)
                {
                    ModelState.AddModelError("", "Deve preencher a data de validade");
                }
            }

            int sequencia; string codigo;
            codigo = pacienteServices.GenerateCodigo(out sequencia);
            paciente.DataRegisto = DateTime.Now;
            paciente.Codigo = codigo;
            paciente.Sequencia = sequencia;
            if (ModelState.IsValid)
            {
                db.Pacientes.Add(paciente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ParentescoId = new SelectList(db.Parentescos, "Id", "Nome", paciente.ParentescoId);
            ViewBag.SeguradoraId = new SelectList(db.Seguradoras, "Id", "Nome", paciente.SeguradoraId);
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome", paciente.SexoId);
            ViewBag.SangueId = new SelectList(db.Sangues, "Id", "Nome", paciente.SangueId);
            ViewBag.Codigo = pacienteServices.GenerateCodigo();

            return View(paciente);
        }

        // GET: Pacientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }

            ViewBag.SangueId = new SelectList(db.Sangues, "Id", "Nome", paciente.SangueId);
            ViewBag.ParentescoId = new SelectList(db.Parentescos, "Id", "Nome", paciente.ParentescoId);
            ViewBag.SeguradoraId = new SelectList(db.Seguradoras, "Id", "Nome", paciente.SeguradoraId);
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome", paciente.SexoId);
            return View(paciente);
        }

        // POST: Pacientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Paciente paciente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paciente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SangueId = new SelectList(db.Sangues, "Id", "Nome", paciente.SangueId);
            ViewBag.ParentescoId = new SelectList(db.Parentescos, "Id", "Nome", paciente.ParentescoId);
            ViewBag.SeguradoraId = new SelectList(db.Seguradoras, "Id", "Nome", paciente.SeguradoraId);
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome", paciente.SexoId);
            return View(paciente);
        }

        // GET: Pacientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        // POST: Pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Paciente paciente = db.Pacientes.Find(id);
            db.Pacientes.Remove(paciente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
