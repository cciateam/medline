﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Consulta;
using MedLine.Models.Identity;

namespace MedLine.Controllers
{
    public class AreaActuacaosController : Controller
    {
        private AppDbContext db = new AppDbContext();


        public ActionResult JsonList(int? tipoAgendamento)
        {
            if (tipoAgendamento == null || tipoAgendamento < 1)
                tipoAgendamento = db.TiposAgendamentos.FirstOrDefault().Id;

            var areas = db.AreasActuacoes.Where(t=>t.TipoAgendamentoId == tipoAgendamento.Value);

            return Json(areas.Select(x => new { Codigo = x.Id, Nome = x.Nome, Valor = x.Valor }), JsonRequestBehavior.AllowGet);
        }


        // GET: AreaActuacaos
        public ActionResult Index()
        {
            var areasActuacoes = db.AreasActuacoes.Include(a => a.TipoAgendamento);
            return View(areasActuacoes.ToList());
        }

        // GET: AreaActuacaos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaActuacao areaActuacao = db.AreasActuacoes.Find(id);
            if (areaActuacao == null)
            {
                return HttpNotFound();
            }
            return View(areaActuacao);
        }

        // GET: AreaActuacaos/Create
        public ActionResult Create()
        {
            ViewBag.TipoAgendamentoId = new SelectList(db.TiposAgendamentos, "Id", "Nome");
            return View();
        }

        // POST: AreaActuacaos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TipoAgendamentoId,Nome,Valor,CID")] AreaActuacao areaActuacao)
        {
            if (ModelState.IsValid)
            {
                db.AreasActuacoes.Add(areaActuacao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoAgendamentoId = new SelectList(db.TiposAgendamentos, "Id", "Nome", areaActuacao.TipoAgendamentoId);
            return View(areaActuacao);
        }

        // GET: AreaActuacaos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaActuacao areaActuacao = db.AreasActuacoes.Find(id);
            if (areaActuacao == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoAgendamentoId = new SelectList(db.TiposAgendamentos, "Id", "Nome", areaActuacao.TipoAgendamentoId);
            return View(areaActuacao);
        }

        // POST: AreaActuacaos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TipoAgendamentoId,Nome,Valor,CID")] AreaActuacao areaActuacao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(areaActuacao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoAgendamentoId = new SelectList(db.TiposAgendamentos, "Id", "Nome", areaActuacao.TipoAgendamentoId);
            return View(areaActuacao);
        }

        // GET: AreaActuacaos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaActuacao areaActuacao = db.AreasActuacoes.Find(id);
            if (areaActuacao == null)
            {
                return HttpNotFound();
            }
            return View(areaActuacao);
        }

        // POST: AreaActuacaos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AreaActuacao areaActuacao = db.AreasActuacoes.Find(id);
            db.AreasActuacoes.Remove(areaActuacao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
