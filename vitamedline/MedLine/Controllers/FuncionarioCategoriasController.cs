﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Funcionarios;
using MedLine.Models.Identity;

namespace MedLine.Controllers
{
    public class FuncionarioCategoriasController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: FuncionarioCategorias
        public ActionResult Index()
        {
            return View(db.FuncionariosCategorias.ToList());
        }

        // GET: FuncionarioCategorias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuncionarioCategoria funcionarioCategoria = db.FuncionariosCategorias.Find(id);
            if (funcionarioCategoria == null)
            {
                return HttpNotFound();
            }
            return View(funcionarioCategoria);
        }

        // GET: FuncionarioCategorias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FuncionarioCategorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome")] FuncionarioCategoria funcionarioCategoria)
        {
            if (ModelState.IsValid)
            {
                db.FuncionariosCategorias.Add(funcionarioCategoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(funcionarioCategoria);
        }

        // GET: FuncionarioCategorias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuncionarioCategoria funcionarioCategoria = db.FuncionariosCategorias.Find(id);
            if (funcionarioCategoria == null)
            {
                return HttpNotFound();
            }
            return View(funcionarioCategoria);
        }

        // POST: FuncionarioCategorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome")] FuncionarioCategoria funcionarioCategoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(funcionarioCategoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(funcionarioCategoria);
        }

        // GET: FuncionarioCategorias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuncionarioCategoria funcionarioCategoria = db.FuncionariosCategorias.Find(id);
            if (funcionarioCategoria == null)
            {
                return HttpNotFound();
            }
            return View(funcionarioCategoria);
        }

        // POST: FuncionarioCategorias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FuncionarioCategoria funcionarioCategoria = db.FuncionariosCategorias.Find(id);
            db.FuncionariosCategorias.Remove(funcionarioCategoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
