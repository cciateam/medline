﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Comum;
using MedLine.Models.Identity;

namespace MedLine.Controllers
{
    public class ParentescoesController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Parentescoes
        public ActionResult Index()
        {
            return View(db.Parentescos.ToList());
        }

        // GET: Parentescoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parentesco parentesco = db.Parentescos.Find(id);
            if (parentesco == null)
            {
                return HttpNotFound();
            }
            return View(parentesco);
        }

        // GET: Parentescoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Parentescoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome")] Parentesco parentesco)
        {
            if (ModelState.IsValid)
            {
                db.Parentescos.Add(parentesco);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(parentesco);
        }

        // GET: Parentescoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parentesco parentesco = db.Parentescos.Find(id);
            if (parentesco == null)
            {
                return HttpNotFound();
            }
            return View(parentesco);
        }

        // POST: Parentescoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome")] Parentesco parentesco)
        {
            if (ModelState.IsValid)
            {
                db.Entry(parentesco).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(parentesco);
        }

        // GET: Parentescoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parentesco parentesco = db.Parentescos.Find(id);
            if (parentesco == null)
            {
                return HttpNotFound();
            }
            return View(parentesco);
        }

        // POST: Parentescoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Parentesco parentesco = db.Parentescos.Find(id);
            db.Parentescos.Remove(parentesco);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
