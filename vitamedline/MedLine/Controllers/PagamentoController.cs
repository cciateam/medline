﻿using MedLine.Models.Comum;
using MedLine.Models.Enums;
using MedLine.Models.Financa;
using MedLine.Models.Identity;
using MedLine.Services.ComumServices;
using MedLine.Services.FinancaServices;
using MedLine.Services.LaboratorioServices;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedLine.Controllers
{
    public class PagamentoController : BaseController
    {
        private AppDbContext db = new AppDbContext();
        private PagamentosServices labServices = new PagamentosServices();

        // GET: Pagamento
        public ActionResult Index()
        {
            var list = labServices.MapPacientesGroup();
            return View(list);
        }

        public ActionResult PagamentosPendentes(int pacienteId)
        {
            var list = labServices.MapPagamentos(pacienteId);
            var paciente = db.Pacientes.Find(pacienteId);
            ViewBag.Paciente = paciente;
            return View(list);
        }

        public ActionResult EfectuarPagamento(int id, int pacienteId)
        {
            var model = db.Pagamentos.Find(id);

            ViewBag.TipoPagamento = new SelectList(Enum.GetValues(typeof(ETipoPagamento)).Cast<ETipoPagamento>().ToList().Select(x => new { Id = x, Value = Extensions.GetDescription(x) }), "Id", "Value");
            var paciente = db.Pacientes.Find(pacienteId);
            ViewBag.Paciente = paciente;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EfectuarPagamento(Pagamento pagamento, int id, int pacienteId)
        {
            var model = db.Pagamentos.Find(id);
            if (pagamento.FormaPagamento == EFormaPagamento.SeguroSaude)
            {
                if (SessionFile.Count() < 1)
                {
                    ModelState.AddModelError("", "Nenhum anexo de comprovativo para o seguro de saúde");
                }
            }
            else
            {
                if ( model.PagamentosDetalhes.Sum(x=>x.Valor)>pagamento.ValorPago)
                {
                    ModelState.AddModelError("", "Nenhum Valor entregue menor");
                }
            }
            if (ModelState.IsValid)
            {
                model.ValorPago = pagamento.ValorPago;
                model.Pago = true;
                model.FormaPagamento = pagamento.FormaPagamento;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                var agendamento = db.Agendamentos.Find(model.AgendamentoId);
                if (agendamento.Estado != EEstadoAgendamento.Finalizada || 
                    agendamento.Estado != EEstadoAgendamento.Consulta || 
                    agendamento.Estado != EEstadoAgendamento.Laboratório)
                {
                    agendamento.Estado = EEstadoAgendamento.Triagem;
                    db.Entry(agendamento).State = EntityState.Modified;
                    db.SaveChanges();
                }
                var analise = agendamento.Analises.FirstOrDefault();
                if (analise != null)
                {
                    foreach (var item in analise.AnalisesExamesLaboratorio)
                    {
                        var lab = db.AnalisesExames.Find(item.Id);
                        lab.EstadoAnaliseExame = EEstadoAnaliseExame.Laboratorio;
                        db.Entry(lab).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                SaveFicheiros(model.Id);
                return RedirectToAction("PagamentosPendentes", new { pacienteId = pacienteId });
            }

            ViewBag.TipoPagamento = new SelectList(Enum.GetValues(typeof(ETipoPagamento)).Cast<ETipoPagamento>().ToList().Select(x => new { Id = x, Value = Extensions.GetDescription(x) }), "Id", "Value");
            var paciente = db.Pacientes.Find(pacienteId);
            ViewBag.Paciente = paciente;
            return View(pagamento);
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            string fileName = "";
            if ((file == null ? false : file.ContentLength > 0))
            {
                fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + Path.GetFileName(file.FileName);
                file.SaveAs(fileName);
                Ficheiro ficheiro = new Ficheiro
                {
                    Descricao = file.FileName,
                    File = System.IO.File.ReadAllBytes(fileName),
                    TipoFicheiro = ETipoFicheiro.Pagamento
                };
                SessionFile.Add(ficheiro);
                return Content(string.Empty);
            }
            return Content("Erro");
        }
        public ActionResult RemoveFile(string filename)
        {
            SessionFile.ForEach(o =>
            {
                if (o.Descricao == filename)
                {
                    SessionFile.Remove(o);
                }
            });
            return base.Content(string.Empty);
        }

    }
}