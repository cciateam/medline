﻿using MedLine.Models.Comum;
using MedLine.Models.Enums;
using MedLine.Models.Identity;
using MedLine.Models.Laboratorio;
using MedLine.Services.FinancaServices;
using MedLine.Services.LaboratorioServices;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedLine.Controllers
{
    public class LaboratorioController : BaseController
    {
        private AppDbContext db = new AppDbContext();
        private LaboratoriosServices labServices = new LaboratoriosServices();

        // GET: Laboratorio
        public ActionResult Index()
        {
            var list = labServices.MapPacientesGroup();
            return View(list);
        }

        public ActionResult ExamesLaboratorio(int pacienteId)
        {
            var list = labServices.MapExames(pacienteId);
            if (list.Count() < 1)
            {
                return RedirectToAction("Index");
            }
            var paciente = db.Pacientes.Find(pacienteId);
            ViewBag.Paciente = paciente;
            return View(list);
        }

        public ActionResult AguardarResultado(int id, int pacienteId)
        {
            try
            {
                var model = db.AnalisesExames.Find(id);
                model.EstadoAnaliseExame = EEstadoAnaliseExame.AguardarResultado;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                TempData["erro"] = "Aconteceu um erro ao gravar os dados. Por favor, tenta novamente.\n" + e.Message;
                //throw;
            }

            return RedirectToAction("ExamesLaboratorio", new { pacienteId = pacienteId });
        }
        public ActionResult AnularExameLaboratório(int id, int pacienteId)
        {
            try
            {
                var model = db.AnalisesExames.Find(id);
                model.EstadoAnaliseExame = EEstadoAnaliseExame.AguardarResultado;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                TempData["erro"] = "Aconteceu um erro ao gravar os dados. Por favor, tenta novamente.\n" + e.Message;
                //throw;
            }

            return RedirectToAction("ExamesLaboratorio", new { pacienteId = pacienteId });
        }

        public ActionResult LancarResultado(int id, int pacienteId)
        {
            var model = db.AnalisesExames.Find(id);
            var paciente = db.Pacientes.Find(pacienteId);
            ViewBag.Paciente = paciente;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LancarResultado(AnaliseExameLaboratorio laboratorio, int id, int pacienteId)
        {
            if (ModelState.IsValid)
            {
                var model = db.AnalisesExames.Find(id);
                model.Resultado = laboratorio.Resultado;
                model.Observacoes = laboratorio.Observacoes;
                model.EstadoAnaliseExame = EEstadoAnaliseExame.Finalizada;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                SaveFicheiros(model.Id);
                return RedirectToAction("ExamesLaboratorio", new { pacienteId = pacienteId });
            }

            var paciente = db.Pacientes.Find(pacienteId);
            ViewBag.Paciente = paciente;
            return View(laboratorio);
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            string fileName = "";
            if ((file == null ? false : file.ContentLength > 0))
            {
                fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + Path.GetFileName(file.FileName);
                file.SaveAs(fileName);
                Ficheiro ficheiro = new Ficheiro
                {
                    Descricao = file.FileName,
                    File = System.IO.File.ReadAllBytes(fileName),
                    TipoFicheiro = ETipoFicheiro.Laboratorio
                };
                SessionFile.Add(ficheiro);
                return Content(string.Empty);
            }
            return Content("Erro");
        }
        public ActionResult RemoveFile(string filename)
        {
            SessionFile.ForEach(o =>
            {
                if (o.Descricao == filename)
                {
                    SessionFile.Remove(o);
                }
            });
            return base.Content(string.Empty);
        }

    }
}