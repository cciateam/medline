﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Consulta;
using MedLine.Models.Identity;

namespace MedLine.Controllers
{
    public class TipoAgendamentoesController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: TipoAgendamentoes
        public ActionResult Index()
        {
            return View(db.TiposAgendamentos.ToList());
        }

        public ActionResult JsonList()
        {
            var tipoAgendamentos = db.TiposAgendamentos.Where(t=>t.ETipoAgendamento != Models.Enums.ETipoAgendamento.Laboratorio).ToList();

            return Json(tipoAgendamentos.Select(x => new { Codigo = x.Id, Nome = x.Nome }), JsonRequestBehavior.AllowGet);
        }


        // GET: TipoAgendamentoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoAgendamento tipoAgendamento = db.TiposAgendamentos.Find(id);
            if (tipoAgendamento == null)
            {
                return HttpNotFound();
            }
            return View(tipoAgendamento);
        }

        // GET: TipoAgendamentoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoAgendamentoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome,ETipoAgendamento")] TipoAgendamento tipoAgendamento)
        {
            if (ModelState.IsValid)
            {
                db.TiposAgendamentos.Add(tipoAgendamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoAgendamento);
        }

        // GET: TipoAgendamentoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoAgendamento tipoAgendamento = db.TiposAgendamentos.Find(id);
            if (tipoAgendamento == null)
            {
                return HttpNotFound();
            }
            return View(tipoAgendamento);
        }

        // POST: TipoAgendamentoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome,ETipoAgendamento")] TipoAgendamento tipoAgendamento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoAgendamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoAgendamento);
        }

        // GET: TipoAgendamentoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoAgendamento tipoAgendamento = db.TiposAgendamentos.Find(id);
            if (tipoAgendamento == null)
            {
                return HttpNotFound();
            }
            return View(tipoAgendamento);
        }

        // POST: TipoAgendamentoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoAgendamento tipoAgendamento = db.TiposAgendamentos.Find(id);
            db.TiposAgendamentos.Remove(tipoAgendamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
