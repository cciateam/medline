﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Comum;
using MedLine.Models.Identity;

namespace MedLine.Controllers
{
    public class FicheiroesController : BaseController
    {
        private AppDbContext db = new AppDbContext();

        // GET: Ficheiroes
        public ActionResult Index()
        {
            return View(db.Ficheiros.ToList());
        }

        // GET: Ficheiroes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ficheiro ficheiro = db.Ficheiros.Find(id);
            if (ficheiro == null)
            {
                return HttpNotFound();
            }
            return View(ficheiro);
        }

        // GET: Ficheiroes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ficheiroes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Descricao,File,TipoFicheiro")] Ficheiro ficheiro)
        {
            if (ModelState.IsValid)
            {
                db.Ficheiros.Add(ficheiro);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ficheiro);
        }

        // GET: Ficheiroes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ficheiro ficheiro = db.Ficheiros.Find(id);
            if (ficheiro == null)
            {
                return HttpNotFound();
            }
            return View(ficheiro);
        }

        // POST: Ficheiroes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Descricao,File,TipoFicheiro")] Ficheiro ficheiro)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ficheiro).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ficheiro);
        }

        // GET: Ficheiroes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ficheiro ficheiro = db.Ficheiros.Find(id);
            if (ficheiro == null)
            {
                return HttpNotFound();
            }
            return View(ficheiro);
        }

        // POST: Ficheiroes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ficheiro ficheiro = db.Ficheiros.Find(id);
            db.Ficheiros.Remove(ficheiro);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
