﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MedLine.Models.Funcionarios;
using MedLine.Models.Identity;
using MedLine.Models.Enums;
using MedLine.Services.AgendamentoServices;

namespace MedLine.Controllers
{
    public class FuncionariosController : Controller
    {
        private AppDbContext db = new AppDbContext();

        public ActionResult JsonList()
        {
            var funcionarios = db.Funcionarios.Where(t=>t.Funcao == EFuncionarioFuncao.ProfissionalSaude);

            return Json(funcionarios.Select(x => new { Codigo = x.Id, Nome = x.Nome }), JsonRequestBehavior.AllowGet);
        }

        // GET: Funcionarios
        public ActionResult Index()
        {
            var funcionarios = db.Funcionarios.Include(f => f.FuncionarioCategoria).Include(f => f.Sexo).Include(f => f.TipoDocumento);
            return View(funcionarios.ToList());
        }

        // GET: Funcionarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcionario funcionario = db.Funcionarios.Find(id);
            if (funcionario == null)
            {
                return HttpNotFound();
            }
            return View(funcionario);
        }

        // GET: Funcionarios/Create
        public ActionResult Create()
        {
            ViewBag.FuncionarioCategoriaId = new SelectList(db.FuncionariosCategorias, "Id", "Nome");
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome");
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos, "Id", "Nome");
            return View();
        }

        // POST: Funcionarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Funcionario funcionario)
        {
            if (ModelState.IsValid)
            {
                db.Funcionarios.Add(funcionario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FuncionarioCategoriaId = new SelectList(db.FuncionariosCategorias, "Id", "Nome", funcionario.FuncionarioCategoriaId);
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome", funcionario.SexoId);
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos, "Id", "Nome", funcionario.TipoDocumentoId);
            return View(funcionario);
        }

        // GET: Funcionarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcionario funcionario = db.Funcionarios.Find(id);
            if (funcionario == null)
            {
                return HttpNotFound();
            }
            ViewBag.FuncionarioCategoriaId = new SelectList(db.FuncionariosCategorias, "Id", "Nome", funcionario.FuncionarioCategoriaId);
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome", funcionario.SexoId);
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos, "Id", "Nome", funcionario.TipoDocumentoId);
            return View(funcionario);
        }

        // POST: Funcionarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Funcionario funcionario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(funcionario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FuncionarioCategoriaId = new SelectList(db.FuncionariosCategorias, "Id", "Nome", funcionario.FuncionarioCategoriaId);
            ViewBag.SexoId = new SelectList(db.Sexos, "Id", "Nome", funcionario.SexoId);
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumentos, "Id", "Nome", funcionario.TipoDocumentoId);
            return View(funcionario);
        }

        // GET: Funcionarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcionario funcionario = db.Funcionarios.Find(id);
            if (funcionario == null)
            {
                return HttpNotFound();
            }
            return View(funcionario);
        }

        // POST: Funcionarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Funcionario funcionario = db.Funcionarios.Find(id);
            db.Funcionarios.Remove(funcionario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
