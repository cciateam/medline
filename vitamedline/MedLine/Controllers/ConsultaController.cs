﻿using MedLine.Models.Identity;
using MedLine.Services.AgendamentoServices;
using MedLine.Services.ComumServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MedLine.Models.Consulta;
using MedLine.Models.Enums;
using MedLine.Services.FinancaServices;
using MedLine.Models.Laboratorio;
using MedLine.Models.Financa;

namespace MedLine.Controllers
{
    public class ConsultaController : Controller
    {
        private AppDbContext db = new AppDbContext();
        private VitaServices vitaServices = new VitaServices();
        private AgendamentoServices agendamentoservice = new AgendamentoServices();
        private PagamentosServices pagamentosServices = new PagamentosServices();

        public ActionResult Index()
        {

            var agendamentos = db.Agendamentos.Include(t => t.Paciente).Include(t => t.Funcionario).Include(t => t.AreaActuacao).OrderBy(t => t.DataConsulta).ToList();
            return View(agendamentos);
        }

        public ActionResult Triagem()
        {
            var list = db.Agendamentos.Where(s => s.Estado == Models.Enums.EEstadoAgendamento.Triagem
                            //&&DbFunctions.TruncateTime(s.Agendamento.DataConsulta) == DbFunctions.TruncateTime(DateTime.Now)
                            );
            return View(list);
        }

        public ActionResult NovaTriagem(int id)
        {
            var agendamento = db.Agendamentos.Find(id);
            ViewBag.Agendamento = agendamento;
            return View();
        }
        [HttpPost]
        public ActionResult NovaTriagem(Triagem model)
        {
            if (ModelState.IsValid)
            {
                db.Triagens.Add(model);
                db.SaveChanges();
                agendamentoservice.ChangeStatus(model.AgendamentoId, EEstadoAgendamento.Consulta);
                return RedirectToAction("Triagem");
            }

            var agendamento = db.Agendamentos.Find(model.AgendamentoId);
            ViewBag.Agendamento = agendamento;
            return View(model);
        }

        [HttpPost]
        public ActionResult FinalizarConsulta(int agendamentoId)
        {
            var agendamento = db.Agendamentos.Find(agendamentoId);
            if(agendamento.Estado == EEstadoAgendamento.Laboratório)
            {
                agendamento.Estado = EEstadoAgendamento.Finalizada;
                db.Entry(agendamento).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GuardarAnalise(Analise analise)
        {
            var agendamento = db.Agendamentos.Find(analise.AgendamentoId);

            if (!db.Analises.Any(t => t.AgendamentoId == agendamento.Id))
            {
                analise.DataInicio = DateTime.Now;


                if (ModelState.IsValid)
                {
                    db.Analises.Add(analise);
                    db.SaveChanges();
                }
            } else
            {
                var model = db.Analises.First(t => t.AgendamentoId == agendamento.Id);
                TryUpdateModel(model);
                db.SaveChanges();
            }

            return RedirectToAction("NovoAtendimento", new { id = analise.AgendamentoId });
        }

        [HttpPost]
        public ActionResult GuardarDiagnostico(AnaliseDiagnostico diag)
        {
            var analise = db.Analises.Find(diag.AnaliseId);
            
            if (!db.AnalisesDiagnosticos.Any(t => t.AnaliseId == analise.Id))
            {
                if (ModelState.IsValid)
                {
                    db.AnalisesDiagnosticos.Add(diag);
                    db.SaveChanges();
                }
            }
            else
            {
                var model = db.AnalisesDiagnosticos.First(t => t.AnaliseId == analise.Id);
                TryUpdateModel(model);
                db.SaveChanges();
            }

            return RedirectToAction("NovoAtendimento", new { id = analise.AgendamentoId });
        }


        public ActionResult NovoAtendimento(int id)
        {
            var agendamento = db.Agendamentos.Include(t => t.Paciente).Include(t => t.Funcionario).Include(t => t.AreaActuacao).FirstOrDefault(t => t.Id == id);

            agendamento.Estado = EEstadoAgendamento.Consulta;
            db.Entry(agendamento).State = EntityState.Modified;
            db.SaveChanges();

            ViewBag.AreaActuacaoId = new SelectList(db.AreasActuacoes.Where(t => t.TipoAgendamentoId == (int)ETipoAgendamento.Laboratorio), "Id", "Nome");

            return View(agendamento);
        }

        [HttpPost]
        public ActionResult AdicionarExame(int AreaActuacaoId, int analiseId)
        {
            var analise = db.Analises.Find(analiseId);
            var agendamento = db.Agendamentos.Find(analise.AgendamentoId);
            var serviceoExame = db.AreasActuacoes.Find(AreaActuacaoId);

            int sequencia = 0;
            var codigo = pagamentosServices.GenerateCodigo(out sequencia);



            AnaliseExameLaboratorio analiseexame = new AnaliseExameLaboratorio
            {
                AreaActuacaoId = serviceoExame.Id,
                DataRegisto = DateTime.Now,
                EstadoAnaliseExame = EEstadoAnaliseExame.Pendente,
                AnaliseId = analiseId,
                PacienteId = agendamento.PacienteId
            };

            db.AnalisesExames.Add(analiseexame);
            db.SaveChanges();

            Pagamento pagamento = new Pagamento
            {
                AgendamentoId = agendamento.Id,
                DataRegisto = DateTime.Now,
                Pago = false,
                SubTotal = serviceoExame.Valor,
                Total = serviceoExame.Valor,
                Sequencia = sequencia,
                Codigo = codigo
            };
            db.Pagamentos.Add(pagamento);
            db.SaveChanges();

            PagamentoDetalhes pgDetalhe = new PagamentoDetalhes();
            pgDetalhe.AreaActuacaoId = serviceoExame.Id;
            pgDetalhe.PagamentoId = pagamento.Id;
            pgDetalhe.Valor = serviceoExame.Valor;
            db.PagamentosDetalhes.Add(pgDetalhe);
            db.SaveChanges();

            agendamento.Estado = Models.Enums.EEstadoAgendamento.Laboratório;
            db.Entry(agendamento).State = EntityState.Modified;
            db.SaveChanges();

            ViewBag.AreaActuacaoId = new SelectList(db.AreasActuacoes.Where(t => t.TipoAgendamentoId == (int)ETipoAgendamento.Laboratorio), "Id", "Nome");
            return RedirectToAction("NovoAtendimento", new { id =  analise.Agendamento.Id });
        }

    }
}