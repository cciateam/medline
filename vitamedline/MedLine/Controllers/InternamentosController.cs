﻿using MedLine.Models.Identity;
using MedLine.Models.Internamentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedLine.Controllers
{
    public class InternamentosController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Internamentos
        public ActionResult Index()
        {
            return View();
        }
        // GET: FuncionarioCategorias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FuncionarioCategorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Internamento model)
        {
            if (ModelState.IsValid)
            {
                db.Internamentos.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

    }
}