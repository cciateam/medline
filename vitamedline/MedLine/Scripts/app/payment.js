﻿/**
 * Created by Sahed Ribeiro on 16/10/18.
 */

$(document).ready(function () {
    $(function () {
        $(".autoNumber").autoNumeric('init', { aSep: '.', aDec: ',' });
        $(".autoNumberUCF").autoNumeric('init', { aSep: '', aDec: ',' });
        $(".onlyNumber").autoNumeric('init', { aSep: '', mDec: "0" });
    });
    $("form").submit(function () {
        $(".autoNumber").val(function () {
            var value = $(this).autoNumeric('get');
            value = value.replace(".", ",");
            return value;
        });
        $(".onlyNumber").val(function () {
            return $(this).autoNumeric('get');
        });
        if ($(this).valid()) {
            dialog = bootbox.dialog(
                 {
                     message: '<div class="text-center "><i class="glyphicon glyphicon-refresh gly-spin glyphicon-xl normal-right-spinner"><br/></i></div>',
                     callback: function () {
                     },
                     closeButton: false
                 });
        }
    });

    $("#ValorPago").on("keyup", function () {
        var valor = $(this).val();
        var total = $("#myTotal").text();
        var result = parseFloat(valor) - parseFloat(total);
        console.log("Valor: " + valor);
        console.log("Total" + total);
        console.log("Result" + result);
        if (result < 0) {
            $("#Troco").val("0");
        } else {
            $("#Troco").val(result);
        }
    });
});

function ConfirmarOperacao(id, url, pacienteId) {
    bootbox.confirm({
        title: "CARESYS",
        message: "Confirmar operação?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-success'
            },
            cancel: {
                label: 'Não',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                window.location.assign('/Pagamento/' + url + '?Id=' + id + '&pacienteId=' + pacienteId);
            }
        }
    });
}
