/**
 * Created by benevideschissanga on 8/17/15.
 */
$(document).ready(function () {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /*  className colors

     className: default(transparent), important(red), chill(pink), success(green), info(blue)

     */


    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events div.external-event').each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });


    /* initialize the calendar
     -----------------------------------------------------------------*/

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,next today'
        },
        lang: 'pt',
        //editable: true,
        eventLimit: true,
        firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
        selectable: true,
        defaultView: 'month',

        axisFormat: 'h:mm',
        //columnFormat: {
        //    month: 'ddd',    // Mon
        //    week: 'ddd d', // Mon 7
        //    day: 'dddd M/d',  // Monday 9/7
        //    agendaDay: 'dddd D'
        //},
        //displayEventEnd: {
        //    month: false,
        //    basicWeek: true,
        //    "default": true
        //},
        //titleFormat: {
        //    month: 'MMMM YYYY', // September 2009
        //    week: "MMMM YYYY", // September 2009
        //    day: 'MMMM YYYY'                  // Tuesday, Sep 8, 2009
        //},

        select: function (start, end) {

            $('#modal-novo-envento #DataConsulta').val(start.format('MM-DD-YYYY'));
            $('#modal-novo-envento #hora').val(start.format('HH:mm'));

            populateDropdownFromJson("PacienteId", "/Pacientes/JsonList", "Selecionar paciente", "Codigo", "Nome");
            populateDropdownFromJson("FuncionarioId", "/Funcionarios/JsonList", "Selecionar profissional", "Codigo", "Nome");
            populateDropdownFromJson("tipoAgendamento", "/TipoAgendamentoes/JsonList", "Selecionar tipo agendamento", "Codigo", "Nome");

            $('#modal-novo-envento').modal('show');
        },
        eventClick: function (event, jsEvent, view) {
            $('#modalTitle').html(event.title);
            $('#anular_agendamento').attr('href', '/Agendamentoes/Delete/' + event.id);

            $('#confirmar_agendamento').attr('href', '/Agendamentoes/Confirm/' + event.id);
            

            $('#modalBody').html(event.area + "<br/>" + event.funcionario + "<br/>" + event.observacoes + "<br/>" + moment(event.start).format('DD/MM/YYYY HH:mm'));
            $('#eventUrl').attr('href', event.url);
            $('#modal-mostrar-evento').modal();
        },

        eventRender: function (event, element) {
            console.log(event);
            console.log(element);

            element.find('.fc-title').append("<br/>" + event.area + "<br/>" + event.funcionario + "<br/>" + moment(event.start).format('DD/MM/YYYY '));
        },

        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },

        eventSources: [
            {
                url: '/Agendamentoes/AgendamentosEventsJson',
                error: function () {
                    console.log('erro ao carregar os eventos');
                }
            }
        ]
    });

   
    $('#tipoAgendamento').on('change', function (e) {
        $('#TotalPagar').text('0.0');
        var ta = $('#tipoAgendamento option:selected').val();
        populateDropdownFromJson("AreaActuacaoId", "/AreaActuacaos/JsonList?tipoAgendamento=" + ta, "Seleciona area", "Codigo", "Nome");
    });

    $('#AreaActuacaoId').on('change', function (e) {
        var valorServico = $('#AreaActuacaoId option:selected').data("valor");
        $('#TotalPagar').text(valorServico);
    });

    $('#submitButton').on('click', function () {
        $("#form-novo-agendamento").submit();
    });
    

    $('.fc-toolbar .fc-right').prepend(
        $('<div class="fc-button-group"><button type="button" class="fc-button fc-state-default fc-corner-left" data-toggle="modal" data-target="#modal-procurar"><i class="pe-7s-search pe-lg"></i></button><button type="button" class="fc-button fc-state-default  fc-corner-right"><i class="pe-7s-attention pe-lg"></i> </button></div>')
    );
  
    function populateDropdownFromJson(id, uri, firstOption, optionValue, optionText) {

        var dropdown = $('#'+id);
        dropdown.empty();

        dropdown.append('<option selected="true" disabled>' + firstOption + '</option>');
        dropdown.prop('selectedIndex', 0);

        const url = uri;

        $.getJSON(url, function (data) {
            $.each(data, function (key, entry) {
                dropdown.append($('<option></option>').attr('value', entry[optionValue]).text(entry[optionText]).attr('data-valor', entry['Valor']));
            })
        });
    }

    //picker de horas
    $(function () {
        $('.datetimepicker').datetimepicker({
            format: 'HH:mm',
            showClose: true,
            defaultDate: moment()
        });
    });
});
