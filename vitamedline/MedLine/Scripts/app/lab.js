﻿/**
 * Created by Sahed Ribeiro on 16/10/18.
 */

$(document).ready(function () {



});

function ConfirmarOperacao(id, url, pacienteId) {
    bootbox.confirm({
        title: "CARESYS",
        message: "Confirmar operação?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-success'
            },
            cancel: {
                label: 'Não',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                window.location.assign('/Laboratorio/' + url + '?Id=' + id + '&pacienteId=' + pacienteId);
            }
        }
    });
}
