﻿
$(document).ready(function () {

    $(".btnAtendimento").click(function () {
        var id = $(this).data("id");
        $.ajax({
            url: '/Pacientes/_ResumoAnaliseMedica',
            //datatype: "html",
            type: "GET",
            //contenttype: 'application/json; charset=utf-8',
            //async: true,
            data:{ id : id },
            success: function (data) {
                debugger;
                console.log(data);
                $("#modal-atendimento").html(data);
                $('#modal-atendimento').modal('show');
                //$('#modal-atendimento').modal('handleUpdate');
            },
            error: function (xhr) {
                alert('error');
            }
        });
    });
    $(".btnReceituario").click(function () {
        var id = $(this).data("id");
        $.ajax({
            url: '/Pacientes/_ResumoPrescricaoMedica',
            //datatype: "html",
            type: "GET",
            //contenttype: 'application/json; charset=utf-8',
            //async: true,
            data:{ id : id },
            success: function (data) {
                debugger;
                console.log(data);
                $("#modal-resumo-atendimento").html(data);
                $('#modal-resumo-atendimento').modal('show');
                //$('#modal-atendimento').modal('handleUpdate');
            },
            error: function (xhr) {
                alert('error');
            }
        });
    });

    $('#modal-atendimento').on('hidden.bs.modal', function (e) {
        $("#modal-atendimento").empty();
    })
    $('#modal-resumo-atendimento').on('hidden.bs.modal', function (e) {
        $("#modal-resumo-atendimento").empty();
    })

});