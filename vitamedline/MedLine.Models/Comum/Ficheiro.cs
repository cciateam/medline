﻿using MedLine.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Comum
{
    public class Ficheiro
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public byte[] File { get; set; }

        public ETipoFicheiro TipoFicheiro { get; set; }
    }
}
