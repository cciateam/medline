﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Comum.ViewModel
{
    public class PacientesViewModel
    {
        public int Id { get; set; }
        public string CodigoPaciente { get; set; }
        public string NomePaciente { get; set; }
        public string TipoCliente { get; set; }
        public string Sexo { get; set; }
        public string Telefone { get; set; }
        public string Idade { get; set; }
    }
}
