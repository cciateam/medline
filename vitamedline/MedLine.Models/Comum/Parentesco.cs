﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Comum
{
    public class Parentesco
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
