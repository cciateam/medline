﻿using MedLine.Models.Enums;
using MedLine.Models.Funcionarios;
using MedLine.Models.Laboratorio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    /// <summary>
    /// Criado assim que se fazer um agendamento
    /// </summary>
    public class Analise
    {
        public int Id { get; set; }

        [Display(Name = "Agendamento")]
        public int AgendamentoId { get; set; }
        public virtual Agendamento Agendamento { get; set; }

        public string QueixaPrincipal { get; set; }
        public string Destalhes { get; set; }
        public string ProblemasCardiacos { get; set; }
        public string ProblemasRespiratorios { get; set; }
        public string ProblemasGastricos { get; set; }
        public string ProblemasRenais { get; set; }
        public string Alergias { get; set; }
        public bool Gravidez { get; set; }
        public bool Diabetes { get; set; }
        public bool Hepatite { get; set; }
        public string UsoMedicamentos { get; set; }

        public DateTime DataInicio { get; set; }
        public DateTime? DataFim { get; set; }

        public virtual ICollection<AnaliseDiagnostico> AnalisesDiagnosticos { get; set; }
        public virtual ICollection<Triagem> Triagens { get; set; }
        public virtual ICollection<AnaliseExameLaboratorio> AnalisesExamesLaboratorio { get; set; }

        public virtual ICollection<AnalisePrescricaoMedica> AnalisesPrescricoesMedicas { get; set; }
    }
}
