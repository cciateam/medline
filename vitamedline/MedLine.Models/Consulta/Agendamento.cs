﻿using MedLine.Models.Enums;
using MedLine.Models.Funcionarios;
using MedLine.Models.Pacientes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    public class Agendamento
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Paciente")]
        public int? PacienteId { get; set; }
        public virtual Paciente Paciente { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Serviço")]
        public int AreaActuacaoId { get; set; }
        public virtual AreaActuacao AreaActuacao { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Profissional de saúde")]
        public int? FuncionarioId { get; set; }
        public virtual Funcionario Funcionario { get; set; }

        public EEstadoAgendamento Estado { get; set; }
        public DateTime DataEstado { get; set; }

        //[Column(TypeName = "datetime2")]
        public DateTime DataRegisto { get; set; }
        //[Column(TypeName = "datetime2")]
        public DateTime DataConsulta { get; set; }
        public string Codigo { get; set; }
        public int Sequencia { get; set; }

        [Display(Name = "Observações")]
        public string Observacoes { get; set; }

        public virtual ICollection<Analise> Analises { get; set; }
        public virtual ICollection<Triagem> Triagens { get; set; }
    }
}
