﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    public class AreaActuacao
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Tipo de agendamento")]
        public int TipoAgendamentoId { get; set; }
        public virtual TipoAgendamento TipoAgendamento { get; set; }

        [Display(Name ="Descrição")]
        public string Nome { get; set; }

        [Display(Name ="Valor Unit.")]
        public decimal Valor { get; set; }

        public string CID { get; set; }
    }
}
