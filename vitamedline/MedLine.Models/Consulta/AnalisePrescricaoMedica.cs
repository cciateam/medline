﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    public class AnalisePrescricaoMedica
    {
        public int Id { get; set; }

        public int AnaliseId { get; set; }
        public virtual Analise Analise { get; set; }

        public int ViaAdministracaoId { get; set; }
        public virtual PrescricaoMedicaViaAdministracao ViaAdministracao { get; set; }

        public int PrescricaoMedicaDosagemId { get; set; }
        public virtual PrescricaoMedicaDosagem PrescricaoMedicaDosagem { get; set; }

        public string Farmaco { get; set; }
        public int Quantidade { get; set; }
        public string Posologia { get; set; }

    }
}
