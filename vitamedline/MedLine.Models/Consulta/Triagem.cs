﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    public class Triagem
    {
        public int Id { get; set; }

        public int AgendamentoId { get; set; }
        public virtual Agendamento Agendamento { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name ="Altura (m)")]
        public decimal Altura { get; set; }
        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Peso (kg)")]
        public decimal Peso { get; set; }
        [Display(Name = "Frequência Cardíaca (bpm)")]
        public string FrequenciaCardiaca { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Temperatura (ºC)")]
        public string Temperatura { get; set; }
        [Display(Name = "Pulsação")]
        public string Pulso { get; set; }
        [Display(Name = "Respiração")]
        public string Respiracao { get; set; }
        [Display(Name = "Tensão Arterial")]
        public string TensaoArterial { get; set; }
        [Display(Name = "Sinais e sintomas")]
        public string SinaisSintomas { get; set; }

        [Display(Name = "Observações")]
        public string Observacoes { get; set; }

    }
}
