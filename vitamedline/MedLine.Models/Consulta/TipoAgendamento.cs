﻿using MedLine.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    public class TipoAgendamento
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public ETipoAgendamento ETipoAgendamento { get; set; }
    }
}
