﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Consulta
{
    public class AnaliseDiagnostico
    {
        public int Id { get; set; }

        public int AnaliseId { get; set; }
        public Analise Analise { get; set; }

        public string Diagnostico { get; set; }
        public string Detalhes { get; set; }
    }
}
