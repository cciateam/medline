﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Internamentos
{
    public class LocalInternamento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeMaxima { get; set; }
        public int QuantidadePaciente { get; set; }
    }
}
