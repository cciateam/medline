﻿
namespace MedLine.Models.Internamentos
{
    public class TipoInternamento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}