﻿using MedLine.Models.Consulta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Internamentos
{
    public class Internamento
    {
        public int Id { get; set; }

        public int AgendamentoId { get; set; }
        public Agendamento Agendamento { get; set; }

        public int LocalInternamentoId { get; set; }
        public LocalInternamento LocalInternamento { get; set; }

        public int TipoInternamentoId { get; set; }
        public TipoInternamento TipoInternamento { get; set; }

        public DateTime DataEntrada { get; set; }
        public DateTime? DataSaida { get; set; }

    }
}
