﻿using MedLine.Models.Comum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Definicao
{
    public class DadosEmpresa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        [Required(ErrorMessage ="Obrigatório")]
        [DataType(DataType.PhoneNumber)]
        public string Telefone { get; set; }
        [Required(ErrorMessage = "Obrigatório")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        //Endereço
        public string Endereco { get; set; }
        public int NumRua { get; set; }

        public int MunicipioId { get; set; }
        public Municipio Municipio { get; set; }

        public byte[] Imagem { get; set; }
    }
}
