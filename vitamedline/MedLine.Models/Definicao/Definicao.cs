﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Definicao
{
    public class Definicao
    {
        public int Id { get; set; }
        public int TempoDeConsulta { get; set; }
    }
}
