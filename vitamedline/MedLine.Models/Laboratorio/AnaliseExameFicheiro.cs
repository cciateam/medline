﻿using MedLine.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Laboratorio
{
    public class AnaliseExameFicheiro
    {
        public int Id { get; set; }

        public int AnaliseExameId { get; set; }
        public virtual AnaliseExameLaboratorio AnaliseExame { get; set; }

        public int FicheiroId { get; set; }
        public virtual Ficheiro Ficheiro { get; set; }

    }
}
