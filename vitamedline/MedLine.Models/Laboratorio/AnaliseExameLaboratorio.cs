﻿using MedLine.Models.Comum;
using MedLine.Models.Consulta;
using MedLine.Models.Enums;
using MedLine.Models.Pacientes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Laboratorio
{
    /// <summary>
    /// Só para as áreas do laboratório
    /// </summary>
    public class AnaliseExameLaboratorio
    {
        public int Id { get; set; }

        public int? AnaliseId { get; set; }
        public virtual Analise Analise { get; set; }

        public int? PacienteId { get; set; }
        public virtual Paciente Paciente { get; set; }

        public int AreaActuacaoId { get; set; }
        public virtual AreaActuacao AreaActuacao { get; set; }

        [Display(Name = "Resultado")]
        public string Resultado { get; set; }
        public DateTime DataRegisto { get; set; }
        public EEstadoAnaliseExame EstadoAnaliseExame { get; set; }
        public string Observacoes { get; set; }

        public virtual ICollection<AnaliseExameFicheiro> Ficheiros { get; set; }
    }
}
