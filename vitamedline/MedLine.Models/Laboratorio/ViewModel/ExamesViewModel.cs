﻿using MedLine.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Laboratorio.ViewModel
{
    public class ExamesViewModel
    {
        public int Id { get; set; }
        public string CodigoPaciente { get; set; }
        public string NomePaciente { get; set; }
        public string CID { get; set; }
        public string ExameDescricao { get; set; }
        public string DataRegisto { get; set; }
        public string HoraRegisto { get; set; }
        public string Estado { get; set; }
        public EEstadoAnaliseExame EEstado { get; set; }

        public string Resultado { get; set; }
    }
}
