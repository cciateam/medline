﻿using MedLine.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Financa
{
    public class PagamentoFicheiro
    {
        public int Id { get; set; }

        public int PagamentoId { get; set; }
        public Pagamento Pagamento { get; set; }

        public int FicheiroId { get; set; }
        public Ficheiro Ficheiro { get; set; }
    }
}
