﻿using MedLine.Models.Consulta;
using MedLine.Models.Pacientes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Financa
{
    public class PagamentoDetalhes
    {
        public int Id { get; set; }

        [Display(Name = "Pagamento")]
        public int PagamentoId { get; set; }
        public virtual Pagamento Pagamento { get; set; }

        [Display(Name = "Serviço")]
        public int? AreaActuacaoId { get; set; }
        public virtual AreaActuacao AreaActuacao { get; set; }

        public decimal Valor { get; set; }

    }
}
