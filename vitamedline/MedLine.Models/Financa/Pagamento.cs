﻿using MedLine.Models.Consulta;
using MedLine.Models.Enums;
using MedLine.Models.Pacientes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Financa
{
    /// <summary>
    /// Relatorio Final
    /// </summary>
    public class Pagamento
    {
        public int Id { get; set; }

        [Display(Name = "# Agendamento")]
        public int? AgendamentoId { get; set; }
        public virtual Agendamento Agendamento { get; set; }

        public string Codigo { get; set; }
        public int Sequencia { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public decimal ValorPago { get; set; }

        public DateTime DataRegisto { get; set; }
        public DateTime? DataPagamento { get; set; }
        public bool Pago { get; set; }
        /// <summary>
        /// Todas as formas de pagamento
        /// </summary>
        public EFormaPagamento FormaPagamento { get; set; }

        public virtual ICollection<PagamentoDetalhes> PagamentosDetalhes { get; set; }
    }
}
