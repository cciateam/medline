﻿using MedLine.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Financa
{
    /// <summary>
    /// Históricos de pagamentos parcelados
    /// </summary>
    public class PagamentoFormasPagamento
    {
        public int Id { get; set; }

        public int PagamentoId { get; set; }
        public Pagamento Pagamento { get; set; }

        public decimal Valor { get; set; }
        public ETipoPagamento TipoPagamento { get; set; }
    }
}
