﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Financa
{
    public class NotaDebito
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public int Sequencia { get; set; }
        public DateTime DataRegisto { get; set; }
    }
}
