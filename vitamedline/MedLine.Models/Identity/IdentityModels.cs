﻿using MedLine.Models.Comum;
using MedLine.Models.Consulta;
using MedLine.Models.Definicao;
using MedLine.Models.Financa;
using MedLine.Models.Funcionarios;
using MedLine.Models.Internamentos;
using MedLine.Models.Laboratorio;
using MedLine.Models.Pacientes;
using MedLine.Models.Seguros;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Identity
{

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
        //}

        /// <summary>
        /// COMUM
        /// </summary>
        public DbSet<Ficheiro> Ficheiros { get; set; }
        public DbSet<Municipio> Municipios { get; set; }
        public DbSet<Parentesco> Parentescos { get; set; }
        public DbSet<Provincia> Provincias { get; set; }
        public DbSet<Sexo> Sexos { get; set; }
        public DbSet<TipoDocumento> TipoDocumentos { get; set; }

        /// <summary>
        /// CONSULTA
        /// </summary>
        public DbSet<Agendamento> Agendamentos { get; set; }
        public DbSet<Analise> Analises { get; set; }
        public DbSet<AnaliseDiagnostico> AnalisesDiagnosticos { get; set; }
        public DbSet<AnalisePrescricaoMedica> AnalisesPrescricaoMedicas { get; set; }
        public DbSet<AreaActuacao> AreasActuacoes { get; set; }
        public DbSet<PrescricaoMedicaDosagem> PrescricoesMedicasDosagens { get; set; }
        public DbSet<PrescricaoMedicaViaAdministracao> PrescricoesMedicasViaAdministracoes { get; set; }
        public DbSet<TipoAgendamento> TiposAgendamentos { get; set; }
        public DbSet<Triagem> Triagens { get; set; }

        /// <summary>
        /// DEFINICAO
        /// </summary>
        public DbSet<DadosEmpresa> DadosEmpresa { get; set; }

        /// <summary>
        /// FINANCA
        /// </summary>
        public DbSet<NotaDebito> NotasDebitos { get; set; }
        public DbSet<Pagamento> Pagamentos { get; set; }
        public DbSet<PagamentoDetalhes> PagamentosDetalhes { get; set; }
        public DbSet<PagamentoFicheiro> PagamentosFicheiros { get; set; }
        public DbSet<PagamentoFormasPagamento> PagamentosFormasPagamentos { get; set; }

        /// <summary>
        /// FUNCIONARIOS
        /// </summary>
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<FuncionarioArea> FuncionariosAreas { get; set; }
        public DbSet<FuncionarioCategoria> FuncionariosCategorias { get; set; }

        /// <summary>
        /// INTERNAMENTO
        /// </summary>
        public DbSet<Internamento> Internamentos { get; set; }
        public DbSet<LocalInternamento> LocalInternamentos { get; set; }
        public DbSet<TipoInternamento> TiposInternamentos { get; set; }

        /// <summary>
        /// LABORATORIO
        /// </summary>
        public DbSet<AnaliseExameLaboratorio> AnalisesExames { get; set; }
        public DbSet<AnaliseExameFicheiro> AnalisesExamesFicheiros { get; set; }

        /// <summary>
        /// PACIENTES
        /// </summary>
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Sangue> Sangues { get; set; }
        public DbSet<PacienteFicheiro> PacientesFicheiros { get; set; }
        public DbSet<Seguradora> Seguradoras { get; set; }


        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
    }

}
