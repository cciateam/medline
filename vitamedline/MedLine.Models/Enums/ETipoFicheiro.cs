﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum ETipoFicheiro
    {
        [Description("Laboratório")]
        Laboratorio=1,
        [Description("Pagamento")]
        Pagamento = 2,
        [Description("Paciente")]
        Paciente = 3,
    }
}
