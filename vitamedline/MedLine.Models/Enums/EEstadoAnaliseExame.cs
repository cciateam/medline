﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum EEstadoAnaliseExame
    {
        [Description("Pendente Pagamento")]
        Pendente = 1,
        [Description("Laboratório")]
        Laboratorio = 2,
        [Description("Aguardar Resultado")]
        AguardarResultado = 3,
        [Description("Finalizado")]
        Finalizada = 4
    }
}
