﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum ETipoPaciente
    {
        [Description("Particular")]
        Particular = 1,
        [Description("Seguro Empresa")]
        [Display(Description ="Seguro Empresa")]
        SeguroEmpresa = 2,
        [Description("Seguro Particular")]
        SeguroParticular = 3
    }
}
