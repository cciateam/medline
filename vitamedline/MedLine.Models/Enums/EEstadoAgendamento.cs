﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum EEstadoAgendamento
    {
        [Description("Agendado")]
        Agendado = 1,
        [Description("Confirmado")]
        Confirmado = 2,
        [Description("Triagem")]
        Triagem = 3,
        [Description("Consulta")]
        Consulta = 4,
        [Description("Laboratório")]
        Laboratório = 5,
        [Description("Finalizado")]
        Finalizada = 6
    }
}
