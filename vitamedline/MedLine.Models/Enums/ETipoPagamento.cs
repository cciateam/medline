﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum ETipoPagamento
    {
        [Description("Dinheiro")]
        Dinheiro = 1,
        [Description("Multicaixa")]
        Multicaixa = 2,
        /// <summary>
        /// Só para Form de Pagamento em Seguro
        /// </summary>
        [Description("Seguro Saúde")]
        SeguroSaude = 3,
        [Description("Visa")]
        Visa = 4,
        [Description("Multicaixa")]
        Mastercard = 5
    }
    public enum EFormaPagamento
    {
        [Description("Dinheiro")]
        Dinheiro = 1,
        [Description("Seguro Saúde")]
        SeguroSaude = 2,
        [Description("Multicaixa")]
        Multicaixa = 3,
        [Description("Visa")]
        Visa = 4,
        [Description("MasterCard")]
        Mastercard = 5
    }
}
