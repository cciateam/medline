﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum EFuncionarioFuncao
    {
        [Description("Administrativa")]
        Admnistrativa = 1,
        [Description("Profissional de Saúde")]
        ProfissionalSaude = 2
    }
}
