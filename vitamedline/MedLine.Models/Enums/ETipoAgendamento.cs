﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Enums
{
    public enum ETipoAgendamento
    {
        [Description("Médico")]
        Medico =1,
        [Description("Laboratório")]
        Laboratorio = 2,
        [Description("Outros")]
        Outros = 3
    }
}
