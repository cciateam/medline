﻿using MedLine.Models.Consulta;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Funcionarios
{
    public class FuncionarioArea
    {
        public int Id { get; set; }

        public int FuncionarioId { get; set; }
        public Funcionario Funcionario { get; set; }

        public int AreaActuacaoId { get; set; }
        public AreaActuacao AreaActuacao { get; set; }
    }
}
