﻿using MedLine.Models.Comum;
using MedLine.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Funcionarios
{
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        [Required(ErrorMessage ="Obrigatório")]
        [Display(Name ="Tipo documento")]
        public int TipoDocumentoId { get; set; }
        public TipoDocumento TipoDocumento { get; set; }

        [Required(ErrorMessage ="Obrigatório")]
        [Display(Name ="Nº Documento")]
        public string NumDocumento { get; set; }

        [Display(Name ="Data nascimento")]
        public DateTime DataNascimento { get; set; }

        [Display(Name = "Sexo")]
        [DefaultValue(null)]
        public int? SexoId { get; set; }
        public virtual Sexo Sexo { get; set; }

        //Endereco/Contacto
        public string Endereco { get; set; }

        [Display(Name = "E-mail")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email inválido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Telefone")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Telefone inválido")]
        [StringLength(13, MinimumLength = 9)]
        public string Telefone1 { get; set; }

        [Display(Name = "Telefone")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Telefone inválido")]
        [StringLength(13, MinimumLength = 9)]
        public string Telefone2 { get; set; }

        //Função
        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name ="Função")]
        public EFuncionarioFuncao Funcao { get; set; }

        [Required(ErrorMessage ="Obrigatório")]
        [Display(Name ="Categoria")]
        public int FuncionarioCategoriaId { get; set; }
        public FuncionarioCategoria FuncionarioCategoria { get; set; }

        [Display(Name ="Salário Base")]
        public decimal? SalarioBase { get; set; }

        [Display(Name ="H.E")]
        public string HE { get; set; }
    }
}
