﻿using MedLine.Models.Comum;
using MedLine.Models.Consulta;
using MedLine.Models.Enums;
using MedLine.Models.Seguros;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Pacientes
{
    public class Paciente
    {
        public int Id { get; set; }

        [DisplayName("Código")]
        public string Codigo { get; set; }
        public int Sequencia { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        public string Nome { get; set; }
        [DisplayName("Tipo paciente")]
        public virtual ETipoPaciente TipoPaciente { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Data de nascimento")]
        public DateTime DataNascimento { get; set; }

        [Display(Name = "Data registo")]
        public DateTime DataRegisto { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Sexo")]
        public int SexoId { get; set; }
        public virtual Sexo Sexo { get; set; }

        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Required(ErrorMessage = "Obrigatório")]
        [Display(Name = "Telefone")]
        [DataType(DataType.PhoneNumber, ErrorMessage ="Telefone inválido")]
        [StringLength(13, MinimumLength = 9,ErrorMessage ="Telefone de ter entre 9 a 13 caracteres")]
        public string Telefone { get; set; }

        [Display(Name ="E-mail")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Email inválido")]
        public string Email { get; set; }

        [DisplayName("Sangue")]
        public int? SangueId { get; set; }
        public Sangue Sangue { get; set; }

        [DisplayName("Observações")]
        public string Observacoes { get; set; }

        //Complementares
        public string Familiar { get; set; }
        [DisplayName("Telefone")]
        public string FamiliarTelefone { get; set; }

        [Display(Name = "Parentesco")]
        public int? ParentescoId { get; set; }
        public virtual Parentesco Parentesco { get; set; }

        //Seguradora
        public int? SeguradoraId { get; set; }
        public virtual Seguradora Seguradora { get; set; }
        [DisplayName("Nº da apólice")]
        public string Apolice { get; set; }
        public DateTime? DataValidade { get; set; }
        [DisplayName("Entidade Empregadora")]
        public string Empresa { get; set; }

        public virtual ICollection<Agendamento> Agendamentos { get; set; }

    }
}
