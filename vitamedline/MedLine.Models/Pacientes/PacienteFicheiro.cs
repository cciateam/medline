﻿using MedLine.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Models.Pacientes
{
    public class PacienteFicheiro
    {
        public int Id { get; set; }

        public int PacienteId { get; set; }
        public Paciente Paciente { get; set; }

        public int FicheiroId { get; set; }
        public Ficheiro Ficheiro { get; set; }
    }
}
