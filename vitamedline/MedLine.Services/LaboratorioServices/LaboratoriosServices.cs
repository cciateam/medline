﻿using AutoMapper;
using MedLine.Models.Enums;
using MedLine.Models.Identity;
using MedLine.Models.Laboratorio;
using MedLine.Models.Laboratorio.ViewModel;
using MedLine.Models.Pacientes;
using MedLine.Services.ComumServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MedLine.Models.Comum.ViewModel;

namespace MedLine.Services.LaboratorioServices
{
    public class LaboratoriosServices
    {

        public List<ExamesViewModel> MapExames(int pacienteId)
        {
            using (var db = new AppDbContext())
            {
                var dbList = db.AnalisesExames
                                    .Where(x => x.EstadoAnaliseExame == EEstadoAnaliseExame.Laboratorio ||
                                    x.EstadoAnaliseExame == EEstadoAnaliseExame.AguardarResultado &&
                                    x.PacienteId == pacienteId
                                    );
                var list = Mapper.Map<List<ExamesViewModel>>(dbList);
                return list;
            }
        }
        public List<PacientesViewModel> MapPacientesGroup()
        {
            using (var db = new AppDbContext())
            {
                var dbList = db.AnalisesExames.Include(x=>x.Paciente)
                                    .Where(x => x.EstadoAnaliseExame == EEstadoAnaliseExame.Laboratorio || x.EstadoAnaliseExame == EEstadoAnaliseExame.AguardarResultado)
                                    .GroupBy(x => x.Paciente)
                                    .Select(x => x.Key)
                                    .AsQueryable()
                                    //.Select(x => new Paciente
                                    //{
                                    //    Codigo = x.First().Paciente.Codigo,
                                    //    Nome = x.First().Paciente.Codigo,
                                    //    TipoPaciente = x.First().Paciente.TipoPaciente,
                                    //    DataNascimento = x.First().Paciente.DataNascimento,
                                    //    Sexo = x.First().Paciente.Sexo,
                                    //    SexoId = x.First().Paciente.SexoId,
                                    //    Telefone = x.First().Paciente.Telefone
                                    //})
                                    ;
                var list = Mapper.Map<List<PacientesViewModel>>(dbList);
                return list;
            }
        }

    }
}
