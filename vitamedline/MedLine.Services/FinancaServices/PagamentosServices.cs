﻿using AutoMapper;
using MedLine.Models.Comum.ViewModel;
using MedLine.Models.Enums;
using MedLine.Models.Identity;
using MedLine.Models.Laboratorio.ViewModel;
using MedLine.Services.ComumServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using MedLine.Models.Financa;

namespace MedLine.Services.FinancaServices
{
    public class PagamentosServices
    {
        private AppDbContext db = new AppDbContext();

        public string GenerateCodigo(out int seq)
        {
            int sequencia = 1;
            using (RepositoryBase vitaServices = new RepositoryBase())
            {
                var list = vitaServices.FindAll<MedLine.Models.Financa.Pagamento>();
                if (list.Any())
                {
                    sequencia = list.Max(x => x.Sequencia) + 1;
                }
            }
            seq = sequencia;
            return sequencia.ToString("D5");
        }
        public string GenerateCodigo()
        {
            int sequencia = 1;
            using (RepositoryBase vitaServices = new RepositoryBase())
            {
                var list = vitaServices.FindAll<MedLine.Models.Financa.Pagamento>();
                if (list.Any())
                {
                    sequencia = list.Max(x => x.Sequencia) + 1;
                }
            }
            return sequencia.ToString("D5");
        }

        public List<Pagamento> MapPagamentos(int pacienteId)
        {
            var dbList = db.Pagamentos.Where(x => !x.Pago && x.Agendamento.PacienteId == pacienteId).ToList();
            //var list = Mapper.Map<List<PagamentoViewModel>>(dbList);
            return dbList;
        }
        public List<PacientesViewModel> MapPacientesGroup()
        {
            using (var db = new AppDbContext())
            {
                var dbList = db.Pagamentos
                                    .Where(x => !x.Pago)
                                    .GroupBy(x => x.Agendamento.Paciente)
                                    .Select(x => x.Key)
                                    .AsQueryable();
                var list = Mapper.Map<List<PacientesViewModel>>(dbList);
                return list;
            }
        }

    }
}
