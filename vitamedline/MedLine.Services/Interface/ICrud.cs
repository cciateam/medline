﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Services.Interface
{
    public interface ICrud : IDisposable
    {

        T Single<T>(Expression<Func<T, bool>> expression) where T : class, new();
        IQueryable<T> FindAll<T>() where T : class, new();
        void Add<T>(T item) where T : class, new();
        void Update<T>(T item) where T : class, new();
        void Delete<T>(T item) where T : class, new();
        void Delete<T>(Expression<Func<T, bool>> expression) where T : class, new();
        //void DeleteAll<T>() where T : class, IEntity, new();
        void CommitChanges();
    }
}
