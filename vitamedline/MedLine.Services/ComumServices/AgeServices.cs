﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Services.ComumServices
{
    public class AgeServices
    {
        public int Years;
        public int Months;
        public int Days;

        public AgeServices() { }
        public AgeServices(DateTime Bday)
        {
            this.Count(Bday);
        }
        public AgeServices(DateTime Bday, DateTime Cday)
        {
            this.Count(Bday, Cday);
        }
        public AgeServices Count(DateTime Bday)
        {
            return this.Count(Bday, DateTime.Today);
        }
        public AgeServices Count(DateTime Bday, DateTime Cday)
        {
            if ((Cday.Year - Bday.Year) > 0 ||
                (((Cday.Year - Bday.Year) == 0) && ((Bday.Month < Cday.Month) ||
                  ((Bday.Month == Cday.Month) && (Bday.Day <= Cday.Day)))))
            {
                int DaysInBdayMonth = DateTime.DaysInMonth(Bday.Year, Bday.Month);
                int DaysRemain = Cday.Day + (DaysInBdayMonth - Bday.Day);

                if (Cday.Month > Bday.Month)
                {
                    this.Years = Cday.Year - Bday.Year;
                    this.Months = Cday.Month - (Bday.Month + 1) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    this.Days = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
                else if (Cday.Month == Bday.Month)
                {
                    if (Cday.Day >= Bday.Day)
                    {
                        this.Years = Cday.Year - Bday.Year;
                        this.Months = 0;
                        this.Days = Cday.Day - Bday.Day;
                    }
                    else
                    {
                        this.Years = (Cday.Year - 1) - Bday.Year;
                        this.Months = 11;
                        this.Days = DateTime.DaysInMonth(Bday.Year, Bday.Month) - (Bday.Day - Cday.Day);
                    }
                }
                else
                {
                    this.Years = (Cday.Year - 1) - Bday.Year;
                    this.Months = Cday.Month + (11 - Bday.Month) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    this.Days = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
            }
            else
            {
                throw new ArgumentException("Birthday date must be earlier than current date");
            }
            return this;
        }

        public string GetAgeDescription(DateTime Bday)
        {
            AgeServices age = this.Count(Bday);
            string ano = age.Years > 1 ? age.Years + " anos " : age.Years == 1 ? age.Years + " ano " : "";
            string mes = age.Months > 1 ? age.Months + " meses " : age.Months == 1 ? age.Months + " mês " : "";
            string dia = age.Days > 1 ? age.Days + " dias " : age.Days == 1 ? age.Days + " dia " : "";
            return ano + mes + dia;
        }
        /**
         * Usage example:
         * ==============
         * DateTime bday = new DateTime(1987, 11, 27);
         * DateTime cday = DateTime.Today;
         * Age age = new Age(bday, cday);
         * Console.WriteLine("It's been {0} years, {1} months, and {2} days since your birthday", age.Year, age.Month, age.Day);
         */
    }
}
