﻿using MedLine.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Services.ComumServices
{
    public class VitaServices
    {
        public static string FromDatesToHoursFormat(DateTime StartDateTime, DateTime? EndDateTime)
        {
            EndDateTime = EndDateTime.HasValue ? EndDateTime.Value : DateTime.Now;
            var durationTimeSpan = EndDateTime.Value.Subtract(StartDateTime);
            var duration =
                (int)durationTimeSpan.TotalHours + ":" +
                durationTimeSpan.Minutes.ToString("00") + ":" +
                durationTimeSpan.Seconds.ToString("00");

            return duration;
        }

        public static string ColorFromState(EEstadoAgendamento estado)
        {
            switch(estado)
            {
                case EEstadoAgendamento.Agendado:
                    return "#0099ff";
                case EEstadoAgendamento.Confirmado:
                    return "#00e600";
                case EEstadoAgendamento.Triagem:
                    return "#ffd400";
                case EEstadoAgendamento.Consulta:
                    return "#ff6600";
                case EEstadoAgendamento.Laboratório:
                    return "#00fffa";
                default:
                    return "#cc3300";
            }
        }
    }

    public static class Extensions
    {
        public static string GetEnumDescription<TEnum>(this Enum value)
        {
            return value.GetType().GetMember(value.ToString()).FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        }
        public static string GetDescription(this Enum value)
        {
            var enumMember = value.GetType().GetMember(value.ToString()).FirstOrDefault();
            var descriptionAttribute =
                enumMember == null
                    ? default(DescriptionAttribute)
                    : enumMember.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
            return
                descriptionAttribute == null
                    ? value.ToString()
                    : descriptionAttribute.Description;
        }
    }
}
