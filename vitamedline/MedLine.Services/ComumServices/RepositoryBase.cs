﻿using MedLine.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using MedLine.Models.Identity;

namespace MedLine.Services.ComumServices
{
    public class RepositoryBase : ICrud
    {
        //private AppDbContext db = AppDbContext.Create();
        private AppDbContext _db;
        protected virtual AppDbContext db
        {
            get
            {
                if (_db == null)
                {
                    _db = new AppDbContext();
                }
                return _db;
            }
        }

        public void Add<T>(T item) where T : class, new()
        {

        }

        public IQueryable<T> FindAll<T>() where T : class, new()
        {
                return db.Set<T>();
        }
        public virtual IQueryable<T> FindAllBy<T>(Expression<Func<T, bool>> expression) where T : class
        {
            if (expression != null)
            {
                using (db)
                {
                    return db.Set<T>().Where(expression);
                }
            }
            else
            {
                throw new ArgumentNullException("Predicate value must be passed to FindAllBy<T>.");
            }
        }
        public void CommitChanges()
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(T item) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public T Single<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            if (expression != null)
            {
                using (db)
                {
                    return db.Set<T>().Where(expression).SingleOrDefault();
                }
            }
            else
            {
                throw new ArgumentNullException("Predicate value must be passed to FindSingleBy<T>.");
            }
        }

        public void Update<T>(T item) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if (db != null) db.Dispose();
        }
    }
}
