﻿using MedLine.Models.Pacientes;
using MedLine.Services.ComumServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedLine.Services.PacienteServices
{
    public class PacienteServices
    {

        public string GenerateCodigo(out int seq)
        {
            int sequencia = 1;
            using (RepositoryBase vitaServices = new RepositoryBase())
            {
                var list = vitaServices.FindAll<Paciente>();
                if (list.Any())
                {
                    sequencia = list.Max(x => x.Sequencia) + 1;
                }
            }
            seq = sequencia;
            return sequencia.ToString("D5");
        }
        public string GenerateCodigo()
        {
            int sequencia = 1;
            using (RepositoryBase vitaServices = new RepositoryBase())
            {
                var list = vitaServices.FindAll<Paciente>();
                if (list.Any())
                {
                    sequencia = list.Max(x => x.Sequencia) + 1;
                }
            }
            return sequencia.ToString("D5");
        }
    }
}
