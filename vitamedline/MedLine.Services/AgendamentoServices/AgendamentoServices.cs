﻿using MedLine.Models.Enums;
using MedLine.Models.Identity;
using MedLine.Services.ComumServices;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedLine.Models.Consulta;

namespace MedLine.Services.AgendamentoServices
{
    public class AgendamentoServices
    {
        public string GenerateCodigo(out int seq)
        {
            int sequencia = 1;
            using (RepositoryBase vitaServices = new RepositoryBase())
            {
                var list = vitaServices.FindAll<MedLine.Models.Consulta.Agendamento>();
                if (list.Any())
                {
                    sequencia = list.Max(x => x.Sequencia) + 1;
                }
            }
            seq = sequencia;
            return sequencia.ToString("D5");
        }
        public string GenerateCodigo()
        {
            int sequencia = 1;
            using (RepositoryBase vitaServices = new RepositoryBase())
            {
                var list = vitaServices.FindAll<MedLine.Models.Consulta.Agendamento>();
                if (list.Any())
                {
                    sequencia = list.Max(x => x.Sequencia) + 1;
                }
            }
            return sequencia.ToString("D5");
        }

        public Agendamento ChangeStatus(int id, EEstadoAgendamento status)
        {
            using (var db = new AppDbContext())
            {
                var model = db.Agendamentos.Find(id);
                model.Estado = status;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return model;
            }
        }
    }
}
